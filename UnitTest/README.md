# 单元测试部分案例

这部分包括7个案例。

## 案例

### 1. 算法和逻辑错误

即检查算法的和内部各个处理逻辑的正确性。

例如：某程序员编写的打印下降三角形的九九乘法表的程序如下：

详见[MultiplicationTable.java](src/main/java/com/jahentao/unitTest/case01/MultiplicationTable.java)


```
public static void printTable() {
	for (int i = 1; i <= 9; i++) {
		for (int j = 1; j<= 9; j++) {
			System.out.print(String.format("%d * %d = %-2d ", i, j, i*j));
		}
		System.out.println();
	}
}
```


通过检查和测试应能发现程序逻辑是错误的：打印出来的不是下降三角形的九九乘法表，而是9*9的方阵。

![UnitTest-case01](pic/case01.png)

单元测试详见[MultiplicationTableTest.java](src/test/java/com/jahentao/unitTest/test/case01/MultiplicationTableTest.java)

### 2. 模块接口错误

对模块自身的接口做正确性检查；确定形式参数个数、数据类型、顺序是否正确；确定返回值类型，检查返回值的正确性。

例如，以调用JDBC接口调用为例。JDBC（Java Database Connection）为Java开发者使用数据库提供了统一的编程接口，它由一组Java类和接口组成。是Java程序与数据库通信的标准API。

其中Connection接口的职责是与特定数据库的连接(会话)，在连接上下文中执行SQL语句并返回结果。DriverManager的getConnection()方法建立在JDBC URL中定义的数据库Connection连接上。如下所示，连接MySQL数据库。

它有三个重载的方法。


```
public static Connection getConnection(String url, Properties info) throws SQLException
public static Connection getConnection(String url, String user, String password) throws SQLException
public static Connection getConnection(String url) throws SQLException
```

在我们编写的调用代码里，参数顺序问题，导致了模块接口调用错误

详见[ModuleInterfaceInvokeExample.java](src/main/java/com/jahentao/unitTest/case02/ModuleInterfaceInvokeExample.java)

```
conn = DriverManager.getConnection(username, password, url);
```

![UnitTest-case02](pic/case02.png)

单元测试详见[ModuleInterfaceInvokeExampleTest.java](src/test/java/com/jahentao/unitTest/test/case02/ModuleInterfaceInvokeExampleTest.java)

### 3. 边界条件错误

检查各种边界条件发生时程序执行是否仍然正确，包括检查判断条件的边界等。

例如，某程序用于实现将百分制成绩转换为五级计分制成绩，代码如下：

```
public static GradeEnum convert(Double score) throws ScoreException{
		if (score > 100 || score < 0) {
			throw new ScoreException("分数输入错误");
		}

		if (score > 90) {
			return GradeEnum.EXCELLENT;
		}else if (score < 90 && score > 80) {
			return GradeEnum.GOOD;
		}else if (score < 80 && score > 70) {
			return GradeEnum.FAIR;
		}else if (score < 70 && score > 60) {
			return GradeEnum.PASS;
		}else
			return GradeEnum.FAIL;
}
```
在我们编写的调用代码里，程序中的判断条件漏掉了相等的情况

详见[ScoreToGradeUtil.java](src/main/java/com/jahentao/unitTest/case03/ScoreToGradeUtil.java)

故而导致了部分预期的单元测试没有通过

![UnitTest-case03](pic/case03.png)

单元测试详见[ScoreToGradeUtilTest.java](src/test/java/com/jahentao/unitTest/test/case03/ScoreToGradeUtilTest.java)

### 4. 独立路径

程序编写时可能存在疏漏，应对照程序详细设计书的要求对程序进行检查和测试，看是否漏掉了某些原本需要的处理逻辑，也就是少了某些应当有的独立路径，或者某些独立路径存在处理错误。

例如，某程序用于实现将百分制成绩转换为五级计分制成绩，代码如下：
```
public static GradeEnum convert(Double score) throws ScoreException{
		if (score > 100 || score < 0) {
			throw new ScoreException("分数输入错误");
		}

		if (score >= 90) {
			return GradeEnum.EXCELLENT;
		}else if (score < 90 && score >= 80) {
			return GradeEnum.GOOD;
		}else if (score < 70 && score >= 60) {
			return GradeEnum.PASS;
		}else
			return GradeEnum.FAIL;
}
```
程序漏掉了score < 80 && score >= 70 的情况

详见[ScoreToGradeUtil.java](src/main/java/com/jahentao/unitTest/case04/ScoreToGradeUtil.java)

故而导致了部分预期的单元测试没有通过

![UnitTest-case04](pic/case04.png)

单元测试详见[ScoreToGradeUtilTest.java](src/test/java/com/jahentao/unitTest/test/case04/ScoreToGradeUtilTest.java)

### 5. 错误处理

单元模块应能预见某些代码运行可能出错的条件和情况，并设置适当的出错处理代码，以便在相关代码行运行出现错误时，能妥善处理，保证整个单元模块处理逻辑的正确性，这种出错处理应当是模块功能的一部分。

以文件处理为例，在操纵文件时可能会遇到各种问题，如文件不存在，没有读权限，文件不可写等等。

```
public void readFile(String path) throws FileNotFoundException, IOException {
	FileInputStream in = new FileInputStream(new File(path));

	int temp = 0;
	try {
		temp = in.read();
		while (temp != -1) {
			System.out.print((char) temp);
			temp = in.read();
       }
	} finally {
		in.close();
	}
}
/**
 * 具体操作，调用了读文件接口
 * @see #readFile(String)
 */
public void operate() {
	try {
		readFile("a.txt");
	} catch (FileNotFoundException e) {
		System.out.println("所需文件不存在！");
	} catch (IOException e) {
		System.out.println("文件读写错误！");
	}
}
```

详见[FileOperateExample.java](src/main/java/com/jahentao/unitTest/case05/FileOperateExample.java)

文件不存在，单元测试异常处理的输出如下：

![UnitTest-case05](pic/case05.png)

单元测试详见[FileOperateExampleTest.java](src/test/java/com/jahentao/unitTest/test/case05/FileOperateExampleTest.java)

若出现下列情况之一，则表明模块的错误处理功能包含有错误或缺陷：出错的描述难以理解；出错的描述不足以对错误定位，不足以确定出错的原因；显示的错误信息与实际的错误原因不符；对错误条件的处理不正确；在对错误进行处理之前，错误条件已经引起系统的干预等。

### 6. 输入数据

应当对输入数据进行正确性、规范性或者合理性检查，经验表明，没有对输入数据进行必要和有效的检查，是造成软件系统不稳定或者执行出问题的主要原因之一。

成绩管理软件，成绩输入模块，对成绩输入数据进行校验。

学生类中定义 详见[Student.java](src/main/java/com/jahentao/unitTest/case06/Student.java)

```
public class Student {
	/**学号*/
	@Length(min=11, max=11)
	private String no;
	/**成绩*/
	@Range(min=0, max=100)
	private BigDecimal score;
	// ...
}
```

录入学生成绩 详见[ScoreRecordingModule.java](src/main/java/com/jahentao/unitTest/case06/ScoreRecordingModule.java)

```
public Student inputScore(String Sno, Float score) {
		// 根据学号，从数据库中获取Student对象
		// ...
		Student student = new Student(Sno); // 假定已获得Student对象

		student.setScore(score);

		return student;
}
```

单元测试校验出输入不合法的地方  详见[ScoreRecordingModuleTest.java](src/test/java/com/jahentao/unitTest/test/case06/ScoreRecordingModuleTest.java)

```
@Test
	public void testInputScore02() {
		Student student = module.inputScore("12345678910", 101F);// 分数超过范围

		Set<ConstraintViolation<Student>> constraintViolations =
				validator.validate(student);

		assertEquals(1, constraintViolations.size());
		assertEquals("需要在0和100之间",constraintViolations.iterator().next().getMessage());

	}

	@Test
	public void testInputScore03() {
		Student student = module.inputScore("1234567890", 98F);// 学号位数不对

		Set<ConstraintViolation<Student>> constraintViolations =
				validator.validate(student);

		assertEquals(1, constraintViolations.size());
		assertEquals("长度需要在11和11之间",constraintViolations.iterator().next().getMessage());

	}

```

### 7. 表达式、SQL语句

应检查程序中的表达式及SQL语句的语法和逻辑的正确性。对表达式应该保证不含二义性，对于容易产生歧义的表达式或运算符优先级，如：&&、||、++、--等，可以采用扩号“（）”运算符避免二义性，这样一方面能够保证代码执行的正确性，同时也能够提高代码的可读性。

例如：职称为工程师或讲师，并且年龄小于35岁的人有资格参与评选，写成表达式应当是

详见[Person.java](src/main/java/com/jahentao/unitTest/case07/Person.java)
```
public boolean hasQualification() {

		if (("工程师".equals(title) || "讲师".equals(title)) && age < 35)			    return true;
		return false;
}

```
不加括号，根据运算符优先级，表达式的意思就不一样了
```
public boolean hasQualification() {

		if ("工程师".equals(title) || "讲师".equals(title) && age < 35)			    return true;
		return false;
}

```
又例如，有以下包含SQL字符串的代码，在单引号处时长会忘记，出现问题。

单元测试 详见[PersonTest.java](src/test/java/com/jahentao/unitTest/test/case07/PersonTest.java)

拼凑的SQL语句如：
```
String sql = "SELECT * FROM USER WHERE USERNAME = '"
				+ username.trim() + "' AND PASSWORD = '" + password.trim() + "'";
```

这里将6、输入数据校验与7、SQL语句共同定制了一个例子

详见 [SQLSpliceExample.java](src/main/java/com/jahentao/unitTest/case07/SQLSpliceExample.java) 等类

SQL语句的拼凑，会引起SQL注入。一般为了防止SQL注入，不使用Statement，而是使用PreparedStatement。

该例子，为了体现出拼凑’ 单引号问题，采用了Statement，故效率不高，安全性存在漏洞。

当然SQL注入，可以在密码输入的校验中进行处理，但为了体现使用Statement潜在的SQL注入问题，允许’单引号和空格等特殊字符。在案例定制过程中，也定制的了SQL注入的案例。

![UnitTest-case07-案例效果](pic/case07-密码校验留漏洞.png)

为方便案例演示，数据库使用H2内存数据库。数据库文件在项目的/db下，数据库名test，其中有数据库表user (id,username,password)

案例效果图

![UnitTest-case07-案例效果](pic/case07-案例效果.png)

SQL 注入，效果如图如下

输入：用户名：什么都可以，密码：123ABC' or '1'='1

![UnitTest-case07-SQL注入原理](pic/case07-SQL注入原理.png)

![UnitTest-case07-SQL注入效果](pic/case07-SQL注入.png)
