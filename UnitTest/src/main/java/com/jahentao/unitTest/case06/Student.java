package com.jahentao.unitTest.case06;

import java.math.BigDecimal;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

public class Student {
	/**学号*/
	@Length(min=11, max=11)
	private String no;
	/**成绩*/
	@Range(min=0, max=100)
	private BigDecimal score;
	
	
	public Student() {
	}

	public Student(@Length(min = 11, max = 11) String no) {
		super();
		this.no = no;
	}

	public Student(@Length(min = 11, max = 11) String no, @Range(min = 0, max = 100) BigDecimal score) {
		super();
		this.no = no;
		this.score = score;
	}


	// 其他属性...

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Float getScore() {
		return this.score.floatValue();
	}

	public void setScore(Float score) {
		this.score = new BigDecimal(Float.toString(score));
	}
	
}