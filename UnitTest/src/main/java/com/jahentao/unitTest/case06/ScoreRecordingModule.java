/**
 * 成绩管理软件，成绩录入模块
 */
package com.jahentao.unitTest.case06;

/**
 * 成绩录入模块，对成绩输入数据进行校验。
 * <p>这是个正面演示的例子
 * @author jahentao
 * @date 2018-4-25
 *
 */
public class ScoreRecordingModule {
	
	/**
	 * 输入学生成绩
	 * @param Sno 学号
	 * @param score 分数
	 */
	public Student inputScore(String Sno, Float score) {
		// 根据学号，从数据库中获取Student对象
		// ...
		Student student = new Student(Sno); // 假定已获得Student对象
		
		student.setScore(score);
		
		return student;
	}
	
}
