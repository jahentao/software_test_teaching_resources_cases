/**
 * 表达式语法和逻辑的正确性
 */
package com.jahentao.unitTest.case07;

/**
 * 人
 * @author jahentao
 * @date 2018-4-25
 *
 */
public class Person {
	
	private String title; // 职称
	private Integer age; // 年龄

	public Person(String title, Integer age) {
		this.title = title;
		this.age = age;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}


	/**
	 * 判断是否有资质
	 */
	public boolean hasQualification() {
		
		if (/*(*/"工程师".equals(title) || "讲师".equals(title)/*)*/ && age < 35) // 没加括号，表达意思变了
			return true;
		return false;
	}
}
