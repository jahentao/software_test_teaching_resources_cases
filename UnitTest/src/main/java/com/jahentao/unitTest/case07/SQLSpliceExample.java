/**
 * SQL拼接中容易出错
 */
package com.jahentao.unitTest.case07;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.validation.ConstraintViolation;

/**
 * SQL拼接。
 * <p>以一个GUI简单的应用场景为例
 * @author jahentao
 * @date 2018-4-25
 *s
 */
public class SQLSpliceExample {
	
	private static SystemLoginModule module;
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable(){ 
	        public void run() { 
	        	createUI(); 
	        } 
		});
		
    }

	protected static void createUI() {
		// 创建 JFrame 实例
        JFrame frame = new JFrame("Example");
        // Setting the width and height of frame
        frame.setSize(300, 190);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        JPanel panel = new JPanel();    
        // 添加面板
        frame.add(panel);
        /* 
         * 调用用户定义的方法并添加组件到面板
         */
        placeComponents(panel);

        // 设置界面可见
        frame.setVisible(true);
	}

	/**
	 * 面板布局
	 * @param panel
	 */
	private static void placeComponents(JPanel panel) {

        // 布局部分我们这边不多做介绍
        // 这边设置布局为 null
        panel.setLayout(null);

        // 创建 JLabel
        JLabel userLabel = new JLabel("Username:");
        userLabel.setBounds(20,20,80,25);
        panel.add(userLabel);

        // 创建文本域用于用户输入
        JTextField userText = new JTextField(20);
        userText.setBounds(100,20,165,25);
        panel.add(userText);

        // 输入密码的文本域
        JLabel passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(20,50,80,25);
        panel.add(passwordLabel);

        // 类似用于输入的文本域
        // 但是输入的信息会以点号代替，用于包含密码的安全性
        JPasswordField passwordText = new JPasswordField(20);
        passwordText.setBounds(100,50,165,25);
        panel.add(passwordText);

        // 创建登录按钮
        JButton loginButton = new JButton("login");
        loginButton.setBounds(185, 80, 80, 25);
        panel.add(loginButton);
        
        // 消息栏
        JLabel messageLabe = new JLabel("");//Warning:
        messageLabe.setForeground(Color.red);
        messageLabe.setBounds(20, 115, 245, 25);
        messageLabe.setBorder(BorderFactory.createLineBorder(Color.red));
        panel.add(messageLabe);
        
        module = new SystemLoginModule();
        		
        // 按钮点击事件
        loginButton.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// 获取输入的用户名密码
				String username = userText.getText();
				String password = new String(passwordText.getPassword());
				
				User userLogining = new User(username, password);
				
				// 校验用户名、密码输入的规范性和合理性
				Set<ConstraintViolation<User>> constraintViolations = 
						SystemLoginModule.validator.validate(userLogining);
				
				if (constraintViolations.size() == 0) {
					messageLabe.setText("");
					// 校验符合规范
					User userLogined = module.login(userLogining);
					if (userLogined != null) {
						JOptionPane.showMessageDialog(null, "Logined", "Login Successfully!", JOptionPane.PLAIN_MESSAGE);
					} else {
						messageLabe.setText("系统没有该用户");
//						JOptionPane.showMessageDialog(null, "Error", "Login Failed!", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					// 提示错误信息
					Iterator<ConstraintViolation<User>> iterator = constraintViolations.iterator();
					
					StringBuffer buffer = new StringBuffer();
					
					ConstraintViolation<User> next = iterator.next();
					buffer.append(next.getMessage());
					while (iterator.hasNext()) {
						buffer.append("; ");
						next = iterator.next();
						buffer.append(next.getMessage());
					}
					
					messageLabe.setText(new String(buffer));
				}
				
			}
        	
		});
    }
	
}
