/**
 * 系统登录模块
 * <p>为方便案例演示，数据库使用H2内存数据库。数据库文件在项目/db下，数据库名test，
 * 其中数据库表user (id,username,password)
 * <p>为了方便，同样没有给存储在数据库中的密码加密或干扰，安全性没有保证
 * <p>数据库中用户，用户名：user，密码：123456@aA
 */
package com.jahentao.unitTest.case07;

import javax.validation.Validation;
import javax.validation.Validator;

/**
 * 系统登录模块，对输入用户名和密码输入的规范性和合理性进行检查
 * <p>这里规定密码的规范：最少6位，包括至少1个大写字母，1个小写字母，1个数字，1个特殊字符
 * @author jahentao
 * @date 2018-4-25
 * 
 */
public class SystemLoginModule {
	
	private UserDAO userDAO = new JdbcUserDAO();
	
	public static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	public User login(User user) {
		// 根据用户名，从数据库中获得存储的User对象
		User userStored = userDAO.findUser(user.getUserName(), user.getPassword());
		
		return userStored != null ? userStored : null;
	}
}
