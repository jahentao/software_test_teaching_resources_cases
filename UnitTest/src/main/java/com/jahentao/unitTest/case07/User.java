package com.jahentao.unitTest.case07;

import javax.validation.constraints.NotBlank;

/**
 * 用户类
 * @author jahentao
 * @date 2018-4-25
 */
public class User {
	private Long id;
	@NotBlank(message = "用户名不能为空")
	/**用户名，要求唯一*/
	private String userName;
	@Password // 自定义hibernate validation注解
	/**密码*/
	private String password;
	
	public User() {
	}

	public User(@NotBlank String userName) {
		this.userName = userName;
	}
	
	public User(@NotBlank String userName, @Password String password) {
		this.userName = userName;
		this.password = password;
	}

	public User(Long id, @NotBlank(message = "用户名不能为空") String userName, @Password String password) {
		this.id = id;
		this.userName = userName;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
