package com.jahentao.unitTest.case07;

/**
 * UserDAO接口定义
 * @author jahentao
 * @date 2018-4-25
 *
 */
public interface UserDAO {
	/**
	 * 根据用户名（唯一）查找用户
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 */
	public User findUser(String username, String password);
}
