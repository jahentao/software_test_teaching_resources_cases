package com.jahentao.unitTest.case07;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {

	private String regexp;
	
	@Override
	public void initialize(Password constraintAnnotation) {
		this.regexp = constraintAnnotation.regexp();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(value == null) {
			return true;
		}
        if(value.matches(regexp)) {
            return true;
        }
        return false;
	}

}
