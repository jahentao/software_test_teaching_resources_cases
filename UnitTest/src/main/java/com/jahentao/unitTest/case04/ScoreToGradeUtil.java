/**
 * 用于实现将百分制成绩转换为五级计分制成绩
 */
package com.jahentao.unitTest.case04;

/**
 * 百分制成绩转换为五级计分制成绩：优秀、良好、中等、及格、不及格
 * @author jahentao
 * @date 2018-4-24
 *
 */
public class ScoreToGradeUtil {
	
	public enum GradeEnum {
		EXCELLENT,//优秀
		GOOD,//良好
		FAIR,//中等
		PASS,//及格
		FAIL//不及格
	}
	
	/**
	 * 成绩转换等级
	 * @param score 分数
	 * @return 等级(五等：优秀、良好、中等、及格、不及格)
	 */
	public static GradeEnum convert(Double score) throws ScoreException{
		if (score > 100 || score < 0) {
			throw new ScoreException("分数输入错误");
		}
		
		// 程序漏掉了score < 80 && score >= 70 的情况
		if (score >= 90) {
			return GradeEnum.EXCELLENT;
		}else if (score < 90 && score >= 80) {
			return GradeEnum.GOOD;
		}/*else if (score < 80 && score >= 70) {
			return GradeEnum.FAIR;
		}*/else if (score < 70 && score >= 60) {
			return GradeEnum.PASS;
		}else
			return GradeEnum.FAIL;
	}

}