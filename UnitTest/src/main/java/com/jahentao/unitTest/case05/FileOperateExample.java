/**
 * 文件操作案例
 */
package com.jahentao.unitTest.case05;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 异常处理，以文件操作为例，在操纵文件时可能会遇到各种问题。
 * 
 * @author jahentao
 * @date 2018-4-24
 */
public class FileOperateExample {

	/**
	 * 读取文件
	 * @param path 在Java IDE 的project中使用相对路径
	 */
	public void readFile(String path) throws FileNotFoundException, IOException {
		FileInputStream in = new FileInputStream(new File(path));
		
		int temp = 0;
		try {
			temp = in.read();
			while (temp != -1) {
				System.out.print((char) temp);       
				temp = in.read();
	       }
		} finally {
			in.close(); 
		}
	}
	
	/**
	 * 具体操作，调用了读文件接口
	 * @see #readFile(String)
	 */
	public void operate() {
		try {
			readFile("a.txt");
		} catch (FileNotFoundException e) {
			System.out.println("所需文件不存在！");
		} catch (IOException e) {
			System.out.println("文件读写错误！");
		}
	}
}
