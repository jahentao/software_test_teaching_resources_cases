/**
 * 打印九九乘法表，需要打印下降三角形的九九乘法表。
 * <p>注意这是一个演示错误的例子
 * <p>需要你通过静态代码审查测试或编写并运行单元测试来发现错误
 */
package com.jahentao.unitTest.case01;

/**
 * 九九乘法表
 * @author jahentao
 * @date 2018-4-24
 */
public class MultiplicationTable {

	/**
	 * 打印九九乘法表
	 */
	public static void printTable() {
		for (int i = 1; i <= 9; i++) {
			for (int j = 1; j<= 9; j++) {
				System.out.print(String.format("%d * %d = %-2d ", i, j, i*j));
			}
			System.out.println();
		}
	}
}
