package com.jahentao.unitTest.case03;

public class ScoreException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ScoreException(String msg) {
		super(msg);
	}
}
