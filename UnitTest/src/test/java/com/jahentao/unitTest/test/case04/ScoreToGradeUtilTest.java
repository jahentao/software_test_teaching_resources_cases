/**
 * 独立路径测试。
 * <p>可以通过静态代码审查。对照程序详细设计书的要求对程序进行检查，查看逻辑是否疏漏。
 * <p>当然也可以通过测试，逻辑覆盖，看是否漏掉了某些原本需要的处理逻辑
 */
package com.jahentao.unitTest.test.case04;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.jahentao.unitTest.case04.ScoreException;
import com.jahentao.unitTest.case04.ScoreToGradeUtil;
import com.jahentao.unitTest.case04.ScoreToGradeUtil.GradeEnum;

/**
 * 独立路径测试
 * @author jahentao
 * @date 2018-4-24
 */
public class ScoreToGradeUtilTest {

	@Test(expected = ScoreException.class)
	public void testConvert01() throws Exception {
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(101D));
	}
	
	@Test
	public void testConvert02() throws Exception {
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(95D));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(90.0));
	}
	
	@Test
	public void testConvert03() throws Exception {
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(86D));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(80.0));
	}
	
	@Test
	public void testConvert04() throws Exception {
		// 因程序逻辑疏漏不通过
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(75.6));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(70.0));
	}
	
	@Test
	public void testConvert05() throws Exception {
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(65.5));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.0));
	}
	
	@Test
	public void testConvert06() throws Exception {
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(54D));
	}
	
	@Test(expected = ScoreException.class)
	public void testConvert07() throws Exception {
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(-8D));
	}

}
