/**
 * 成绩录入模块单元测试
 */
package com.jahentao.unitTest.test.case06;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jahentao.unitTest.case06.ScoreRecordingModule;
import com.jahentao.unitTest.case06.Student;

/**
 * 成绩录入是否对成绩输入数据进行校验
 * @author jahentao
 * @date 2018-4-25
 *
 */
public class ScoreRecordingModuleTest {
	
	private ScoreRecordingModule module;
	
	private static Validator validator;

	@BeforeClass
	public static void setUpValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}
	
	@Before
	public void setUp() throws Exception {
		module = new ScoreRecordingModule();
	}

	@Test
	public void testInputScore01() {
		Student student = module.inputScore("12345678910", 86.5F);
		
		Set<ConstraintViolation<Student>> constraintViolations =
			validator.validate(student);
		
		assertEquals(0, constraintViolations.size());
		assertEquals(new Float(86.5), student.getScore());
	}
	
	@Test
	public void testInputScore02() {
		Student student = module.inputScore("12345678910", 101F);// 分数超过范围
		
		Set<ConstraintViolation<Student>> constraintViolations =
				validator.validate(student);
		
		assertEquals(1, constraintViolations.size());
		assertEquals("需要在0和100之间",constraintViolations.iterator().next().getMessage());
		
//		assertEquals(new Float(101), student.getScore());
	}
	
	@Test
	public void testInputScore03() {
		Student student = module.inputScore("1234567890", 98F);// 学号位数不对
		
		Set<ConstraintViolation<Student>> constraintViolations =
				validator.validate(student);
		
		assertEquals(1, constraintViolations.size());
		assertEquals("长度需要在11和11之间",constraintViolations.iterator().next().getMessage());
		
	}

}
