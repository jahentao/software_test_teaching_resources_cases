/**
 * 异常处理
 */
package com.jahentao.unitTest.test.case05;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.unitTest.case05.FileOperateExample;

/**
 * 异常处理的单元测试
 * @author jahentao
 * @date 2018-4-24
 */
public class FileOperateExampleTest {
	
	private FileOperateExample example;

	@Before
	public void setUp() throws Exception {
		example = new FileOperateExample();
	}

	/**
	 * 文件操作，要操作的文件不存在
	 * <p>程序能否运行足矣判断操作是否正确，查看异常处理的输出，故未添加断言
	 */
	@Test
	public void testOperate() {
		example.operate();
	}

}
