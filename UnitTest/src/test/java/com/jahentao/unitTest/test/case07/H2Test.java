package com.jahentao.unitTest.test.case07;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class H2Test {
	public static void main(String[] a) throws Exception {
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.getConnection("jdbc:h2:C:\\Users\\jahentao\\git\\software_test_teaching_resources_cases\\UnitTest\\db\\test", "sa", "");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM user");
		while (rs.next()) {
			System.out.println(rs.getString("username") + "," + rs.getString("password"));
		}
		conn.close();
	}
}
