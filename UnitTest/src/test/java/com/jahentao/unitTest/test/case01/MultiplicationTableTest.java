/**
 * 编写并运行单元测试来发现算法和逻辑错误
 */
package com.jahentao.unitTest.test.case01;

import org.junit.Test;

import com.jahentao.unitTest.case01.MultiplicationTable;

/**
 * 九九乘法表单元测试
 * @author jahentao
 * @date 2018-4-24
 */
public class MultiplicationTableTest {

	/**
	 * 运行单元测试，发现打印出来的不是下降三角形的九九乘法表，而是9*9的方阵。
	 * <p>故而，发现了算法和逻辑错误
	 * <p>程序仅需运行就可以看出是否达到预期功能，故未添加断言
	 */
	@Test
	public void testPrintTable() {
		MultiplicationTable.printTable();
	}

}
