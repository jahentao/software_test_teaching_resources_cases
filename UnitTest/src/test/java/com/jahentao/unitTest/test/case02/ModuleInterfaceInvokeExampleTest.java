/**
 * 编写并运行单元测试来发现模块接口调用错误
 */
package com.jahentao.unitTest.test.case02;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.unitTest.case02.ModuleInterfaceInvokeExample;

/**
 * 模块接口调用案例测试
 * <p>调用JDBC接口连接MySQL数据库
 * @author jahentao
 * @date 2018-4-24
 */
public class ModuleInterfaceInvokeExampleTest {
	
	private ModuleInterfaceInvokeExample example;

	@Before
	public void setUp() throws Exception {
		example = new ModuleInterfaceInvokeExample();
	}

	/**
	 * 运行单元测试，发现运行抛出异常
	 * <p>程序能否运行足矣判断模块接口调用是否正确，故未添加断言
	 */
	@Test
	public void test() throws Exception {
		example.connectAndOperateMysql();
	}

}
