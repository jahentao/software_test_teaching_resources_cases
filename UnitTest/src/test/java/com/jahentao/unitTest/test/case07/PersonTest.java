package com.jahentao.unitTest.test.case07;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.unitTest.case07.Person;

/**
 * 单元测试表达式的语法和逻辑的正确性
 * @author jahentao
 * @date 2018-4-25
 *
 */
public class PersonTest {
	
	private Person person;
	private Person person2;

	@Before
	public void setUp() throws Exception {
		person = new Person("工程师", 34);
		person2 = new Person("讲师", 43);
	}

	@Test
	public void testHasQualification() {
		assertTrue(person.hasQualification());
		assertFalse(person2.hasQualification());
	}

}
