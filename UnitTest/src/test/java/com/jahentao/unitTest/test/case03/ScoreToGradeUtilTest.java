/**
 * 编写并运行单元测试来进行逻辑覆盖
 */
package com.jahentao.unitTest.test.case03;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jahentao.unitTest.case03.ScoreException;
import com.jahentao.unitTest.case03.ScoreToGradeUtil;
import com.jahentao.unitTest.case03.ScoreToGradeUtil.GradeEnum;

/**
 * 测试边界条件
 * @author jahentao
 * @date 2018-4-24
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScoreToGradeUtilTest {

	@Test(expected = ScoreException.class)
	public void testConvert01() throws Exception {
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(101D));
	}
	
	@Test
	public void testConvert02() throws Exception {
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(100.0));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(99.9));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(96D));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(90.1));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(90.0));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(89.9));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(86D));
	}
	
	@Test
	public void testConvert03() throws Exception {
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(90.1));
		assertEquals(GradeEnum.EXCELLENT, ScoreToGradeUtil.convert(90.0));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(89.9));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(86D));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(80.1));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(80.0));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(79.9));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(76D));
	}
	
	@Test
	public void testConvert04() throws Exception {
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(80.1));
		assertEquals(GradeEnum.GOOD, ScoreToGradeUtil.convert(80.0));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(79.9));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(76D));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(70.1));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(70.0));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(69.9));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(66D));
	}
	
	@Test
	public void testConvert05() throws Exception {
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(70.1));
		assertEquals(GradeEnum.FAIR, ScoreToGradeUtil.convert(70.0));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(69.9));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(66D));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.1));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.0));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(59.9));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(56D));
	}
	
	@Test
	public void testConvert06() throws Exception {
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.1));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.0));
		assertEquals(GradeEnum.PASS, ScoreToGradeUtil.convert(60.1));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(59.9));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(56D));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(50.1));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(40.0));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(39.9));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(26D));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(11D));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(8.8));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(0.1));
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(0.0));
	}

	@Test(expected = ScoreException.class)
	public void testConvert07() throws Exception {
		assertEquals(GradeEnum.FAIL, ScoreToGradeUtil.convert(-8.88));
	}
	
}
