package com.jahentao.integrationTest.test.case05.other;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case05.other.ModuleA;

public class ModuleATest {
	
	private ModuleA module;

	@Before
	public void setUp() throws Exception {
		module = new ModuleA();
	}

	@Test
	public void testAnnualToDaily01() {
		// 年利率为 3%, 保留 5 位小数，日利率约为 0.00008
		double dailyInterestRate = module.annualToDaily(0.03, 5);
		assertEquals(Double.doubleToLongBits(0.00008), // 转换为长整型，比较两个浮点数
				Double.doubleToLongBits(dailyInterestRate));
	}
	
	@Test
	public void testAnnualToDaily02() {
		// 年利率为 3%, 保留 7 位小数，日利率约为 0.0000822
		double dailyInterestRate = module.annualToDaily(0.03, 7);
		assertEquals(Double.doubleToLongBits(0.0000822), // 转换为长整型，比较两个浮点数
				Double.doubleToLongBits(dailyInterestRate));
	}

}
