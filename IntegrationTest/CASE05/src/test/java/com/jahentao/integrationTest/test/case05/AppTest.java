package com.jahentao.integrationTest.test.case05;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case05.App;

/**
 * 用户使用该应用
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class AppTest {
	
	private App app;

	@Before
	public void setUp() throws Exception {
		app = new App();
	}

	@Test
	public void testRun() {
		double x = 10000; // 实际值
		double delta_x = 3.1415926; // 用户观测误差
		// 用户使用应用
		double y_with_delta = app.run(x + delta_x); // 获取到带误差的结果
		// 实际的真实结果
		double y = app.run(x);
		// 比较x、y相对误差被放大的倍数
		double relative_error_y = (y_with_delta - y) / y;
		double relative_error_x = delta_x / x;
		System.out.println(Math.abs( relative_error_y / relative_error_x));
	}

}
