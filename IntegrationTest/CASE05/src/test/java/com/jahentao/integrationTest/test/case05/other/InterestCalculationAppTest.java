package com.jahentao.integrationTest.test.case05.other;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case05.other.InterestCalculationApp;

public class InterestCalculationAppTest {
	
	private InterestCalculationApp app;

	@Before
	public void setUp() throws Exception {
		app = new InterestCalculationApp();
	}

	@Test
	public void testCalculate() {
		// 设年利率为 3%
		// 某客户存款 1 亿元，存期 100 天
		
		// 以保留5位小数计算
		double interest1 = app.calculate(0.03, 5, 100, 100000000);
		BigDecimal b1 = new BigDecimal(interest1);
		double result1 = b1.setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue(); // 计算结果保留5位小数，
																				// 保持前后精度相同
		System.out.println(result1); // 800000.0  80万
		
		// 以保留7位小数计算
		double interest2 = app.calculate(0.03, 7, 100, 100000000);
		BigDecimal b2 = new BigDecimal(interest2);
		double result2 = b2.setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue(); // 计算结果保留7位小数，
																				// 保持前后精度相同
		System.out.println(result2); // 822000.0  82.2万
		
		System.out.println(result2 - result1); // 相差 2.2 万
	}

}
