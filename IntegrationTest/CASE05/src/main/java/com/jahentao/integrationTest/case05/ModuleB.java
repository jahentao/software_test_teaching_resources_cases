package com.jahentao.integrationTest.case05;

/**
 * 模块B
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleB {

	public static double operate(double x) {
		// 模块B内部进行处理
		// ...
		double temp = 7 * x * x * x;
		// 继续处理
		// ...
		double y = temp;
		// 返回处理结果
		return y;
	}

}
