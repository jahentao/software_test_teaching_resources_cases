package com.jahentao.integrationTest.case05.other;

/**
 * 模块B
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleB {

	/**
	 * 计算总利息
	 * <p> 总利息 = money × dailyInterestRate × days
	 * @param days 存款期限
	 * @param money 本金
	 * @param dailyInterestRate 日利率
	 * @return 总利息
	 */
	public double calculateInterestByDay(int days, double money, double dailyInterestRate) {
		return money * dailyInterestRate * days;
	}
}
