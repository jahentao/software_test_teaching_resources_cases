/**
 * 一个应用
 */
package com.jahentao.integrationTest.case05;

/**
 * 该应用处理过程中，会经过多个模块处理，得到最终结果
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class App {

	/**
	 * 用户拿到这个应用，输入x，获取返回结果。对用户来说，它就是一个黑盒，不关心应用内部的处理。
	 * @param x 输入参数
	 * @return 结果
	 */
	public double run(double x) {
		// 应用内部进行处理
		// ...
		// 调用模块A
		return ModuleA.operate(x); 
	}
	
}
