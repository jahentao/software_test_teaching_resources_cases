package com.jahentao.integrationTest.case05.other;

import java.math.BigDecimal;

/**
 * 模块A
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleA {

	/**
	 * 由年利率计算得出单日的利率，计算结果提供给模块 B
	 * <p> 日利率 = 年利率 ÷ 365
	 * @param annualInterestRate 年利率
	 * @param number 保留 number 位小数
	 * @return
	 */
	public double annualToDaily(double annualInterestRate, int number) {
		BigDecimal b = new BigDecimal(annualInterestRate / 365);
		return b.setScale(number, BigDecimal.ROUND_HALF_UP).doubleValue(); // BigDecimal.ROUND_HALF_UP表示四舍五入
	}
}
