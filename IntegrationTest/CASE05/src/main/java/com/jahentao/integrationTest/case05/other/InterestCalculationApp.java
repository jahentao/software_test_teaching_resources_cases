package com.jahentao.integrationTest.case05.other;

/**
 * 利息计算程序
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class InterestCalculationApp {
	
	private ModuleA moduleA = new ModuleA();
	private ModuleB moduleB = new ModuleB();

	/**
	 * 输入年利率、保留小数位后位数、存款天数和本金，计算总利息
	 * @param annualInterestRate 年利率
	 * @param number 保留小数位后位数
	 * @param days 存款天数
	 * @param money 本金
	 * @return 总利息
	 */
	public double calculate(double annualInterestRate, int number, int days, double money) {
		// 通过模块A，获得日利率
		double dailyInterestRate = moduleA.annualToDaily(annualInterestRate, number);
		// 计算结果提供给模块 B，得出总的利息
		return moduleB.calculateInterestByDay(days, money, dailyInterestRate);
	}
	
}
