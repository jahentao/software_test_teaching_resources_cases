package com.jahentao.integrationTest.case05;
/**
 * 模块A
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleA {

	public static double operate(double x) {
		// 模块A内部进行处理
		// ...
		double temp = 3 * x;
		// 调用模块B
		double y = ModuleB.operate(temp);
		// 继续处理
		// ...
		// 返回处理结果
		return y;
	}

}
