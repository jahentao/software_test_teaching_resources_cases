/**
 * 客户端的屏幕显示界面
 */
package com.jahentao.integrationTest.case03.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.jahentao.integrationTest.case03.common.ScreenCapture;
import com.jahentao.integrationTest.case03.common.ScreenHandler;


public class ScreenCAPJFrame extends JFrame{
	
	private JLabel label;
	private ScreenHandler handler;

	private static final long serialVersionUID = 8798555226941756815L;
	
	public ScreenCAPJFrame(int delay){
		handler = ScreenHandler.getScreenHandlerInstance();
		handler.start();
		init(delay);
	}
	
	public void init(int delay){
		this.setTitle("录屏");
		this.setBounds(0, 0, ScreenCapture.width, ScreenCapture.height); // 这里为了好看，设置了一个比例，这个比例要提出到构造函数中
		label = new JLabel();
		label.setBounds(0, 0, this.getWidth(), this.getHeight());
		this.add(label);
		this.setVisible(true);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(-1);
			}
		});
		
		// 定时间隔刷新界面帧
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run() {
				updateFrame();
			}
		}, 0, delay);
	}
	
	/**
	 * 更新画面帧
	 */
	public void updateFrame(){
		Icon icon = handler.getNextFrame();
		if(icon!=null) {
			label.setIcon(icon);
			repaint();
		}
	}
}
