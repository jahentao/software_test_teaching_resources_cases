/**
 * 屏广软件的服务器端,设置服务器端抓取画面的时间间隔，与客户端刷新界面时间间隔相同
 */
package com.jahentao.integrationTest.case03.server;

import java.util.Timer;

import com.jahentao.integrationTest.case03.common.ScreenCapture;


public class AppServer {

	/**
	 * 服务器端
	 * <p>实时抓取屏幕</p>
	 * @param args 第一个参数为目标主机IP地址
	 */
	public static void main(String[] args) {
		ScreenCapture captureTask = new ScreenCapture(args[0]);
		Timer timer = new Timer();
		timer.schedule(captureTask, 1000, 100); // 100ms 抓一次屏幕
	}

}
