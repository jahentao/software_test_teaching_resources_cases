package com.jahentao.integrationTest.case03.client;

/**
 * 屏广软件的客户端,设置的刷新界面时间间隔，与服务器端抓取画面的时间间隔相同
 */

public class AppClient {

	public static void main(String[] args) {
		new ScreenCAPJFrame(100); //100ms间隔 刷新画面
	}

}
