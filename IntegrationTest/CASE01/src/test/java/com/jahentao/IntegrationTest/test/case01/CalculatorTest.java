package com.jahentao.IntegrationTest.test.case01;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.IntegrationTest.case01.Calculator;

/**
 * 计算器 父类 Calculator 功能测试
 * @author jahentao
 * @date 2018-4-26
 *
 */
public class CalculatorTest {
	
	private Calculator calculator;

	@Before
	public void setUp() throws Exception {
		calculator = new Calculator();
	}

	@Test
	public void testAddAll01() {
		calculator.addAll(new int[]{1,2,3});
		assertEquals(6, calculator.getSum());
	}
	
	@Test
	public void testAddAll02() {
		calculator.addAll(new int[]{1,2,3});
		assertEquals(6, calculator.getSum());
		calculator.clear();
		calculator.addAll(new int[]{1,2,4});
		assertEquals(7, calculator.getSum());
	}

}
