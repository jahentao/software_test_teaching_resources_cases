package com.jahentao.IntegrationTest.test.case01;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.IntegrationTest.case01.RealTimeCalculator;

/**
 * 计算器 子类 ChildCalculator 功能测试
 * @author jahentao
 * @date 2018-4-26
 *
 */
public class RealTimeCalculatorTest {
	
	private RealTimeCalculator childCalculator;

	@Before
	public void setUp() throws Exception {
		childCalculator = new RealTimeCalculator();
	}

	@Test
	public void testAddAll01() {
		childCalculator.addAll(new int[]{1,2,3});
		assertEquals(6, childCalculator.getSum());
	}
	
	@Test
	public void testAddAll02() {
		childCalculator.addAll(new int[]{1,2,3});
		assertEquals(6, childCalculator.getSum());
		
		childCalculator.addAll(new int[]{1,2,4});
		assertEquals(7, childCalculator.getSum());
	}

}
