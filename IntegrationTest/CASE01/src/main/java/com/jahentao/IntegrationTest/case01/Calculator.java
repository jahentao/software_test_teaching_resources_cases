/**
 * 计算器 基类
 */
package com.jahentao.IntegrationTest.case01;

import java.util.ArrayList;
import java.util.List;

/**
 * 计算器 基类
 * <p>仅提供计算所有输入整型变量的和的计算功能
 * <p>如果你看见逻辑上的疏漏，那只是为了下一步改进的需要
 * @author jahentao
 * @date 2018-4-26
 */
public class Calculator {
	
	protected int sum;// 注意：field变量，我之所以不用Integer，而是用值类型int，
					  // 是因为对于继承的子类，int sum 是切实在栈内存中有分配空间用于存储的
					  // 而Integer sum 继承自父类后，只是一个没指向的引用
	
	protected List<Integer> array = new ArrayList<Integer>();
	
	public int getSum() {
		for (int number : array) {
			sum += number;
		}
		return sum;
	}
	
	/**
	 * 添加一个数
	 * @param number 数
	 */
	public void add(int number) {
		array.add(number);
	}


	/**
	 * 将输入数字添加到内部数组中
	 * <p>对使用者来说，addAll就是能够添加数字，具体是怎么添加的，应该不用关心。
	 * @param numbers 输入数组
	 */
	public void addAll(int[] numbers) {
	    for (int number : numbers) {
	    	add(number);
	    }
	}
	
	/**
	 * 基类开发者修改基类，清除上次计算的影响
	 */
	public void clear() {
		array.clear();
		sum = 0;
	}
}
