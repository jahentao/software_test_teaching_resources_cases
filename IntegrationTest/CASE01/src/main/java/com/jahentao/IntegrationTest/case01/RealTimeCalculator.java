/**
 * 计算器 子类
 */
package com.jahentao.IntegrationTest.case01;

/**
 * 实时计算器
 * <p>加入数据的同时，就在实时计算结果
 * <p>需要对addAll方法重写
 * <p>如果你看见逻辑上的不合理，请先容忍这种不合理，故事发展需要
 * @author jahentao
 * @date 2018-4-26
 *
 */
public class RealTimeCalculator extends Calculator {

	/**
	 * 重写add，在添加数据的同时进行计算
	 */
	@Override
	public void add(int number) {
		array.add(number);
		sum += number;
	}

	/**
	 * 编写子类的开发者，修改子类，来解决发现的问题
	 */
	@Override
	public int getSum() {
		return sum;
	}
	
	/**
	 * 编写子类的开发者，修改子类，不将clear的职责交给用户，替用户处理这个问题
	 */
	@Override
	public void addAll(int[] numbers) {
		clear();
		
		for (int number : numbers) {
	    	add(number);
	    }
	}

}
