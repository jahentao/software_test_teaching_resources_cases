package com.jahentao.integrationTest.case09.beans;

public class Grade {

    private int id;

    private int courseId;

    private int studentId;

    private String score;

    @Override
    public String toString() {
        return "Grade{" +
                "score=" + score +
                ", studentId=" + studentId +
                ", courseId=" + courseId +
                ", id=" + id +
                '}';
    }

    public Grade(int courseId, int studentId, String score) {
        this.courseId = courseId;
        this.studentId = studentId;
        this.score = score;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
