package com.jahentao.integrationTest.case09.utils;

import com.jahentao.integrationTest.case09.Session;
import com.jahentao.integrationTest.case09.beans.Student;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentInfoUtils {
    private JdbcUtils jdbcUtils;

    public StudentInfoUtils() {
        jdbcUtils = new JdbcUtils();
        jdbcUtils.getConnection();
    }

    public boolean saveStudentInfo(Student student) {
//

        System.out.println("修改前的参数"+student);

        String sql = "update student set age = ? ,sex = ? ,birthday = ?, address = ? , phone = ?, email = ?,studentClass = ? where studentCode = ? ";
        List<Object> params = new ArrayList<>();
        params.add(student.getAge());
        params.add(student.getSex());
        params.add(student.getBirthday());
        params.add(student.getAddress());
        params.add(student.getPhone());
        params.add(student.getEmail());
        params.add(student.getStudentClass());
        params.add(student.getStudentCode());

        boolean flag = false;
        try {
            flag = jdbcUtils.updateByPreparedStatement(sql, params);
            if (flag == true) {
                Student newStudent = findStudentInfoByUsername(((Student)Session.userInfo).getUsername());
                Session.userInfo = newStudent;
                System.out.println("修改后的值" + newStudent);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flag;

    }

    public Student findStudentInfoByUsername(String username) {
        String sql = "select * from student where username = ?";
        List<Object> params = new ArrayList<>();
        params.add(username);
        Student student = null;
        try {
            student = jdbcUtils.findSimpleRefResult(sql, params, Student.class);
            if (student != null) {
                // 更新session中的对象
                Session.userInfo = student;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return student;
    }


    /**
     * 获得所有学生信息
     * @return
     */
    public List<Map<String, Object>> getAllStudentsInfo() {
        String sql = "select * from student";
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            list = jdbcUtils.findModeResult(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    public boolean deleteStudentById(int id) {
        String sql = "delete from student where id = ?";
        List<Object> params = new ArrayList<>();
        params.add(id);
        boolean flag = false;
        try {
            flag = jdbcUtils.updateByPreparedStatement(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean saveStudentInfoByMap(Map<String, Object> map) {
        String sql = "";
        if (map.containsKey("id".toUpperCase())) {
            sql = "update student set username = ?,password = ?,studentCode = ? ,name = ?, " +
                    "studentClass = ? , age = ?,sex = ?,birthday = ?,address = ?,phone = ? ," +
                    "email = ? where id = ?";
        } else {
            sql = "insert into student(username,password,studentCode,name,studentClass,age,sex," +
                    "birthday,address,phone,email) values (?,?,?,?,?,?,?,?,?,?,?)";
        }
        List<Object> params = new ArrayList<>();
        params.add(map.get("username".toUpperCase()));
        params.add(map.get("password".toUpperCase()));
        params.add(map.get("studentCode".toUpperCase()));
        params.add(map.get("name".toUpperCase()));
        params.add(map.get("studentClass".toUpperCase()));
        params.add(Integer.parseInt(map.get("age".toUpperCase()).toString().trim()));
        params.add(map.get("sex".toUpperCase()));
        params.add(map.get("birthday".toUpperCase()));
        params.add(map.get("address".toUpperCase()));
        params.add(map.get("phone".toUpperCase()));
        params.add(map.get("email".toUpperCase()));
        if (map.containsKey("id".toUpperCase())) {
            params.add(map.get("id".toUpperCase()));
        }

        boolean flag = false;
        try {
            flag = jdbcUtils.updateByPreparedStatement(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (jdbcUtils != null) {
            jdbcUtils.releaseConn();
            jdbcUtils = null;

        }
        System.out.println(this.getClass().toString() + "销毁了");
    }
}
