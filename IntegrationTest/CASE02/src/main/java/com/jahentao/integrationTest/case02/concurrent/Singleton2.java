package com.jahentao.integrationTest.case02.concurrent;

/**
 * 懒加载，单例模式
 * <p>低效
 * <p>在单机，多线程并发情景下
 * @author jahentao
 *
 */
public class Singleton2 {
	
	private static volatile Singleton2 instance = null;
	
	private Singleton2() {
	}

	public static synchronized Singleton2 getInstance() {
		if (instance == null) {
			instance = new Singleton2();
		}
		return instance;
	}
}