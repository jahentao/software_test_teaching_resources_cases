package com.jahentao.integrationTest.case02;

/**
 * 懒加载单例模式
 * <p>非并发情形下
 * @author jahentao
 *
 */
public class Singleton {
	
	private static Singleton instance = null;
	
	private Singleton() {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
}
