package com.jahentao.integrationTest.case02.module;

import com.jahentao.integrationTest.case02.concurrent.Singleton;

/**
 * 模块A
 * @author jahentao
 * @dete 2018-5-18
 */
public class ModuleA implements Runnable {

	@Override
	public void run() {
		// ...
		// 需要获取单例
		Singleton instance = Singleton.getInstance();
		System.out.println(instance);
		// ...
	}

}
