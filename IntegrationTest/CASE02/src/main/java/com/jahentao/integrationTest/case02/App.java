package com.jahentao.integrationTest.case02;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jahentao.integrationTest.case02.module.ModuleA;
import com.jahentao.integrationTest.case02.module.ModuleB;
import com.jahentao.integrationTest.case02.module.ModuleC;

/**
 * 补充案例测试
 * @author jahentao
 * @date 2018-5-18
 */
public class App {

	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(3);
		// 这三个模块中获取的单例，内存地址应该是相同的
		pool.execute(new ModuleA());
		pool.execute(new ModuleB());
		pool.execute(new ModuleC());
		pool.shutdown();
	}

}
