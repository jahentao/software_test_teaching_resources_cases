package com.jahentao.integrationTest.case06;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * 集成测试
 * <p>模块B调用模块A，模块B集成模块A，对模块B功能进行测试
 * <p>发现了模块B中调用模块A时，参数顺序错误
 * <p>而这在模块A的单元测试时是不能被发现的
 * @author jahentao
 * @date 2018-4-27
 * @see com.jahentao.integrationTest.case06.ModuleATest
 *
 */
public class ModuleBTest {

	private ModuleA moduleA;
	private ModuleB moduleB;
	
	@Before
	public void setUp() throws Exception {
		moduleA = new ModuleA();
		moduleB = new ModuleB();
		moduleB.setModuleA(moduleA); // 集成测试
	}

	@Test
	public void testOperate() {
		String result = moduleB.operate("软件测试教学资源-集成测试案例6模块B的调用","测试");
		assertEquals("软件教学资源-集成案例6模块B的调用", result); // 断言错误，因为模板B中调用模块A时，参数顺序错误
	}

}
