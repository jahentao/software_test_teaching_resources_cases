package com.jahentao.integrationTest.case06;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * 模块A 单元测试
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleATest {
	
	private ModuleA module;
	
	@Before
	public void setUp() throws Exception {
		module = new ModuleA();
	}

	@Test
	public void testOperate() {
		String result = module.operate("software testing course needs integration test cases", "test");
		assertEquals("software ing course needs integration  cases", result);
	}

}
