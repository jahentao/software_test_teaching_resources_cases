package com.jahentao.integrationTest.case06;

/**
 * 模块B 的开发者和模块A的开发者不是同一个人
 * <p>在模块B开发时，模块A提供了调用接口
 * <p>模块B和模块A同时开发，没有进行集成测试
 * @author jahentao
 * @date 2018-4-27
 *
 */
public class ModuleB {
	
	private ModuleA moduleA;
	
	public void setModuleA(ModuleA moduleA) {
		this.moduleA = moduleA;
	}


	/**
	 * 模块B的具体处理操作中，调用了模块A的接口
	 */
	public String operate(String str, String exclude) {
		// 模块 B 调用模块 A 时参数位置写反了
		return moduleA.operate(exclude, str);
	}

}
