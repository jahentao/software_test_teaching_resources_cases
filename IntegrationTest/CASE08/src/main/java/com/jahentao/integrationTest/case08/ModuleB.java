package com.jahentao.integrationTest.case08;

/**
 * 模块B，已占用资源Y，申请资源X
 * 
 * @author jahentao
 *
 */
public class ModuleB extends Thread {

	@Override
	public void run() {
		// 获取资源X
		try {
			ResourceY resourceY = App.resourceYPool.getItem();
		} catch (InterruptedException e1) {
			System.out.println("资源Y获取失败");
			e1.printStackTrace();
		}

		System.out.println(Thread.currentThread().getName() + " - 模块B已获取资源Y");
		
		// 模拟其他操作耗时
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// 申请资源X，去进行其他操作
		try {
			ResourceX resourceX = App.resourceXPool.getItem();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + " - 模块B已获取资源X");
	}

}
