package com.jahentao.integrationTest.case08;

/**
 * 模拟死锁
 * @author jahentao
 */
public class App {
	
	// 为方便演示死锁，资源限定为1
	static Pool<ResourceX> resourceXPool = new Pool<ResourceX>(1);
	static Pool<ResourceY> resourceYPool = new Pool<ResourceY>(1);
	
    public static void main( String[] args ) {
    	// 在资源池里放进资源，不能超过资源池的最大容量
    	resourceXPool.setItems(new ResourceX[1]);
    	resourceYPool.setItems(new ResourceY[1]);
    	
    	new ModuleA().start();
    	new ModuleB().start();
    }
}
