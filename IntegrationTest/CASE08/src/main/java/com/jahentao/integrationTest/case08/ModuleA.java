package com.jahentao.integrationTest.case08;

/**
 * 模块A，已占用资源X，申请资源Y
 * 
 * @author jahentao
 *
 */
public class ModuleA extends Thread {
	
	@Override
	public void run() {
		// 获取资源X
		try {
			ResourceX resourceX = App.resourceXPool.getItem();
		} catch (InterruptedException e1) {
			System.out.println("资源X获取失败");
			e1.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + " - 模块A已获取资源X");
		
		// 模拟其他操作耗时
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// 申请资源Y，去进行其他操作
		try {
			ResourceY resourceY = App.resourceYPool.getItem();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + " - 模块A已获取资源Y");
	}
	
}
