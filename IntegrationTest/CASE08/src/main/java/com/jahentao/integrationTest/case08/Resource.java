package com.jahentao.integrationTest.case08;

/**
 * 资源基类
 * @author jahentao
 *
 */
public class Resource {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
