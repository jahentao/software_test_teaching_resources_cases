/**
 * 计费模块
 */
package com.jahentao.integrationTest.case10;

/**
 * 计费模块
 * <p>计费模块中重量的数据单位默认为公斤
 * @author jahentao
 * @date 2018-4-28
 *
 */
public class ChargingModule {

	public static final String UNIT = "KILOGRAM (kg)"; // 公斤

	private double unitPrice; // 单价：多少元/公斤
	
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}


	/**
	 * 计费
	 * @param weight 重量
	 * @return 总价
	 */
	public double charge(double weight) {
		return weight * unitPrice;
	}
	
}