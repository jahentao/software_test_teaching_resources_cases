package com.jahentao.integrationTest.case10;

/**
 * 某收费软件系统
 * @author jahentao
 * @date 2018-4-28
 *
 */
public class App {
	
	private ChargingModule chargingModule = new ChargingModule(); // 计费模块
	private WeightingModule weightingModule = new WeightingModule(); // 称重模块
	
	/**
	 * 程序运行
	 * @return 返回付款金额
	 */
	public double run() {
		double weight = weightingModule.getWeight();
		
		// 假定按照每公斤8元的计费标准
		chargingModule.setUnitPrice(8);
		// 计算总价
		double totoalPrice = chargingModule.charge(weight);
		
		return totoalPrice;
	}
}

