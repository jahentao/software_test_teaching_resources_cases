/**
 * 称重模块
 */
package com.jahentao.integrationTest.case10;

/**
 * 称重模块
 * <p>称重模块中重量单位为克
 * @author jahentao
 * @date 2018-4-28
 *
 */
public class WeightingModule {

	public static final String UNIT = "GRAM (g)"; // 克

	/**
	 * 获取称重结果
	 * @return
	 */
	public double getWeight() {
		return 1500; // 设称重模块称重的结果为1500
	}
	
}
