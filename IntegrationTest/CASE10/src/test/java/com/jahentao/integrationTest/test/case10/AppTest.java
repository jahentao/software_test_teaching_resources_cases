package com.jahentao.integrationTest.test.case10;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case10.App;

/**
 * 该收费软件单元测试
 * @author jahentao
 * @date 2018-4-28
 */
public class AppTest {
	
	private App app;
	
	@Before
	public void setUp() {
		app = new App();
	}
	
	@Test
	public void testRun() {
		// 原本以为12 元，然而计算得出的结果为需要缴费12000元
		assertEquals(Double.doubleToLongBits(12),  // 转换为长整型，比较两个双精度浮点数
				Double.doubleToLongBits(app.run()));
	}
}
