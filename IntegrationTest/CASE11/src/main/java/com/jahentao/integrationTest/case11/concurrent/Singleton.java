package com.jahentao.integrationTest.case11.concurrent;

/**
 * 懒加载，单例模式
 * <p>双重检查锁
 * <p>在单机，多线程并发情景下
 * @author jahentao
 *
 */
public class Singleton {
	
	private static volatile Singleton instance = null;
	
	private Singleton() {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			synchronized (Singleton.class) {
				if (instance == null) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
}