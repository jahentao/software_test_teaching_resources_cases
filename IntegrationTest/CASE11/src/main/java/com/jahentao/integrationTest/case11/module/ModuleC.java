package com.jahentao.integrationTest.case11.module;

import com.jahentao.integrationTest.case11.concurrent.Singleton;

/**
 * 模块C
 * @author jahentao
 * @dete 2018-5-18
 */
public class ModuleC implements Runnable {

	@Override
	public void run() {
		// ...
		// 需要获取单例
		Singleton instance = Singleton.getInstance();
		System.out.println(instance);
		// ...
	}

}
