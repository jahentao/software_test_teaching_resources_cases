package com.jahentao.integrationTest.case07.error;

/**
 * 模拟应用
 * @author jahentao
 * @date 2018-5-14
 */
public class App {

	public static void main(String[] args) {
		
		Ticket ticket = new Ticket("复仇者联盟3：无尽战争", 100); // 该票现有余票100张
		
		for (int i = 0; i < 1; i++) {
			BookTicketThread bookTicketThread = new BookTicketThread(ticket);
			bookTicketThread.start();
		}
		for (int i = 0; i < 1; i++) {
			RefundTicketThread refundTicketThread = new RefundTicketThread(ticket);
			refundTicketThread.start();
		}
		
	}
	
}
