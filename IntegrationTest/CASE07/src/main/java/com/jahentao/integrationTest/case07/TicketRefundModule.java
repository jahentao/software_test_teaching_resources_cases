package com.jahentao.integrationTest.case07;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * 退票模块
 * <p>负责退票业务，每退一张票，票数增加一张
 * @author jahentao
 * @date 2018-5-13
 */
public class TicketRefundModule implements Runnable{

	/** 工厂模式，创建线程 */
    private ThreadFactory threadFactory;
	/** 线程池，线程池中的线程用来退票 */
	private final ExecutorService pool;
	
	public TicketRefundModule(int poolSize) {
		threadFactory = Executors.defaultThreadFactory();
	    pool = Executors.newFixedThreadPool(poolSize);
	}

	@Override
	public void run() {
		
	}

}
