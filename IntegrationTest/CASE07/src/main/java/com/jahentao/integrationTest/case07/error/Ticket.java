package com.jahentao.integrationTest.case07.error;

/**
 * 票
 * @author jahentao
 * @date 2018-5-14
 */
public class Ticket {

	/** 当前票数*/
	private Integer ticketNum;
	/** 票名 */
	private String ticketName;
	
	public Ticket(String ticketName, Integer totalTicketNum) {
		this.ticketName = ticketName;
		this.ticketNum = totalTicketNum;
	}
	
	public Integer getTicketNum() {
		return ticketNum;
	}
	public void setTicketNum(Integer ticketNum) {
		this.ticketNum = ticketNum;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	
}
