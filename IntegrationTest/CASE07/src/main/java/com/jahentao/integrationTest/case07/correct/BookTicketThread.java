package com.jahentao.integrationTest.case07.correct;

/**
 * 购票线程
 * @author jahentao
 * @date 2018-5-14
 */
public class BookTicketThread extends Thread {
	
	private Ticket ticket;
	
	public BookTicketThread(Ticket ticket) {
		this.ticket = ticket;
	}
	public Ticket getTicket() {
		return ticket;
	}
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	@Override
	public void run() {
		for (int i = 0; i < 3; i++) { // 卖1张票
			synchronized (ticket) {
				int ticketNum = ticket.getTicketNum();
				try {
					Thread.sleep(100); // 模拟其他处理时长
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				ticket.setTicketNum(ticketNum - 1);
				System.out.println("卖掉1张，现有余票：" + ticket.getTicketNum());
			}
		}
	}

}
