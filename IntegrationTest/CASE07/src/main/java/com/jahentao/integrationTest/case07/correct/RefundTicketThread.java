package com.jahentao.integrationTest.case07.correct;

/**
 * 退票线程
 * @author jahentao
 * @date 2018-5-14
 */
public class RefundTicketThread extends Thread {

	private Ticket ticket;
	
	public RefundTicketThread(Ticket ticket) {
		this.ticket = ticket;
	}

	@Override
	public void run() {
		for (int i = 0; i < 1; i++) { // 退1张票
			synchronized (ticket) {
				int ticketNum = ticket.getTicketNum();
				ticket.setTicketNum(ticketNum + 1);
				System.out.println("退票1张，现有余票：" + ticket.getTicketNum());
			}
		}
	}
	
}
