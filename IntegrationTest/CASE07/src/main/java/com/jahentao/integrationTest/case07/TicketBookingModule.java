package com.jahentao.integrationTest.case07;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import com.jahentao.integrationTest.case07.entity.Ticket;

/**
 * 购票模块
 * <p>负责售票，每卖出一张票，对应票的票数减少一张
 * <p>购票模块也是一个线程，提供卖票服务，在服务器端等待客户端请求
 * @author jahentao
 * @date 2018-5-13
 */
public class TicketBookingModule implements Runnable{
	
	/** 工厂模式，创建线程 */
    private ThreadFactory threadFactory;
	/** 线程池，线程池中的线程用来卖票 */
	private final ExecutorService pool;
	
	/** 票 */
	private Map<String, Ticket> map;
	
	/**
	 * 初始化购票模块
	 */
	public TicketBookingModule(int poolSize) throws IOException {
		threadFactory = Executors.defaultThreadFactory();
	    pool = Executors.newFixedThreadPool(poolSize);
	}

	@Override
	public void run() {
		
	}

	public Map<String, Ticket> getMap() {
		return map;
	}
	public void setMap(Map<String, Ticket> map) {
		this.map = map;
	}
	
}
