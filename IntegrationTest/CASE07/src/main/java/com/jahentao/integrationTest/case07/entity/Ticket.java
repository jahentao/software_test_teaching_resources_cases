package com.jahentao.integrationTest.case07.entity;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 票
 * @author jahentao
 * @date 2018-5-14
 */
public class Ticket {

	/** 总票数 */
	private Integer totalTicketNum;
	/** 剩余票数：更新数值 原子性
	 *  如果改为数据库，ACID的特性由数据库维护
	 */
	private AtomicInteger ticketNum;
	/** 票名 */
	private String ticketName;
	
	public Ticket(String ticketName, Integer totalTicketNum) {
		this.totalTicketNum = totalTicketNum;
		this.ticketName = ticketName;
		this.ticketNum = new AtomicInteger(totalTicketNum);
	}
	
	public Integer getTotalTicketNum() {
		return totalTicketNum;
	}
	public void setTotalTicketNum(Integer totalTicketNum) {
		this.totalTicketNum = totalTicketNum;
	}
	public AtomicInteger getTicketNum() {
		return ticketNum;
	}
	public void setTicketNum(AtomicInteger ticketNum) {
		this.ticketNum = ticketNum;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	
}
