package com.jahentao.integrationTest.test.case07.error;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case07.error.BookTicketThread;
import com.jahentao.integrationTest.case07.error.Ticket;

public class BookTicketThreadTest {
	
	private BookTicketThread bookTicketThread;

	@Before
	public void setUp() throws Exception {
		bookTicketThread = new BookTicketThread(new Ticket("复仇者联盟3：无尽战争", 10));
	}

	@Test
	public void testRun() {
		bookTicketThread.run();
		assertEquals(9, bookTicketThread.getTicket().getTicketNum().intValue());
	}

}
