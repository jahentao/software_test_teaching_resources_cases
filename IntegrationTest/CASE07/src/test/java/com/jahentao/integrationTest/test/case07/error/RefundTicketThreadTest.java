package com.jahentao.integrationTest.test.case07.error;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jahentao.integrationTest.case07.error.RefundTicketThread;
import com.jahentao.integrationTest.case07.error.Ticket;


public class RefundTicketThreadTest {

	private RefundTicketThread refundTicketThread;
	
	@Before
	public void setUp() throws Exception {
		refundTicketThread = new RefundTicketThread(new Ticket("复仇者联盟3：无尽战争", 10));
	}

	@Test
	public void testRun() {
		refundTicketThread.run();
		assertEquals(11, refundTicketThread.getTicket().getTicketNum().intValue());
	}

}
