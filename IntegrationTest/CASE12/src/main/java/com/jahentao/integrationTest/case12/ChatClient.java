package com.jahentao.integrationTest.case12;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

public class ChatClient extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -6056260699202978657L;

	private final JTextField uriField;
	private final JButton connect;
	private final JButton close;
	private final JTextArea ta;
	private final JTextField chatField;
	private final JButton btnNewButton;
	private WebSocketClient cc;

	public ChatClient( String defaultlocation ) {
		super( "WebSocket聊天客户端" );
		setResizable(false);
		Container c = getContentPane();
		getContentPane().setLayout(null);

		uriField = new JTextField();
		uriField.setBounds(107, 10, 143, 24);
		uriField.setText( defaultlocation );
		c.add( uriField );

		connect = new JButton( "连接" );
		connect.setBounds(258, 9, 63, 27);
		connect.addActionListener( this );
		c.add( connect );

		close = new JButton( "关闭" );
		close.setBounds(317, 9, 63, 27);
		close.addActionListener( this );
		close.setEnabled( false );
		c.add( close );

		JScrollPane scroll = new JScrollPane();
		scroll.setBounds(14, 138, 366, 214);
		c.add( scroll );
		ta = new JTextArea();
		ta.setEditable(false);
		scroll.setViewportView(ta);

		chatField = new JTextField();
		chatField.setBounds(10, 70, 299, 24);
		chatField.setText( "" );
		chatField.addActionListener( this );
		c.add( chatField );
		
		JLabel lblNewLabel = new JLabel("服务器URI：");
		lblNewLabel.setBounds(10, 13, 98, 18);
		getContentPane().add(lblNewLabel);
		
		btnNewButton = new JButton("发送");
		btnNewButton.addActionListener(this);
		btnNewButton.setBounds(317, 69, 63, 27);
		getContentPane().add(btnNewButton);
		
		JLabel label = new JLabel("聊天室信息：");
		label.setBounds(10, 107, 98, 18);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("文本消息：");
		label_1.setBounds(10, 44, 83, 18);
		getContentPane().add(label_1);

		setPreferredSize( new Dimension(400, 400) );
		setSize( new Dimension(400, 400) );

		addWindowListener( new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing( WindowEvent e ) {
				if( cc != null ) {
					cc.close();
				}
				dispose();
			}
		} );

		setLocationRelativeTo( null );
		setVisible( true );
	}

	public void actionPerformed( ActionEvent e ) {

		if( e.getSource() == chatField || e.getSource() == btnNewButton) {
			if( cc != null ) {
				cc.send( chatField.getText() );
				chatField.setText( "" );
				chatField.requestFocus();
			}

		} else if( e.getSource() == connect ) {
			try {
				cc = new WebSocketClient( new URI( uriField.getText() ), new Draft_6455() ) {

					@Override
					public void onMessage( String message ) {
						ta.append( "收到信息：" + message + "\n" );
						ta.setCaretPosition( ta.getDocument().getLength() );
					}

					@Override
					public void onOpen( ServerHandshake handshake ) {
						ta.append( "您已连接到聊天服务器：" + getURI() + "\n" );
						ta.setCaretPosition( ta.getDocument().getLength() );
					}

					@Override
					public void onClose( int code, String reason, boolean remote ) {
						ta.append( "您已经断开与服务器：" + getURI() + "的连接； 错误码: " + code + " " + reason + "\n" );
						ta.setCaretPosition( ta.getDocument().getLength() );
						connect.setEnabled( true );
						uriField.setEditable( true );
						close.setEnabled( false );
					}

					@Override
					public void onError( Exception ex ) {
						ta.append( "出现异常 ...\n" + ex + "\n" );
						ta.setCaretPosition( ta.getDocument().getLength() );
						ex.printStackTrace();
						connect.setEnabled( true );
						uriField.setEditable( true );
						close.setEnabled( false );
					}
				};

				close.setEnabled( true );
				connect.setEnabled( false );
				uriField.setEditable( false );
				cc.connect();
			} catch ( URISyntaxException ex ) {
				ta.append( uriField.getText() + " 不是一个有效的WebSocket的URI\n" );
			}
		} else if( e.getSource() == close ) {
			cc.close();
		}
	}

	public static void main( String[] args ) {
//		WebSocketImpl.DEBUG = true;
		String location;
		if( args.length != 0 ) {
			location = args[ 0 ];
			System.out.println( "服务器地址指定为 \'" + location + "\'" );
		} else {
			location = "ws://localhost:8887";
			System.out.println( "服务器地址没有指定：默认为 \'" + location + "\'" );
		}
		new ChatClient( location );
	}
}
