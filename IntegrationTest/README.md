# 集成测试部分案例

这部分包括13个案例。

## 结构说明

集成测试每个案例都独立设为一个子模块，确保不相互干扰。

## 案例

### 1. 父类修改影响多个子类

有的模块相依性是内在的，典型的如继承关系。

继承作为面向对象三大特性之一，在给程序设计带来巨大便利的同时，也带来了弊端。比如使用继承会给程序带来侵入性，程序的可移植性降低，增加了对象间的耦合性，如果一个类被其他的类所继承，则当这个类需要修改时，必须考虑到所有的子类，并且父类修改后，所有涉及到子类的功能都有可能会产生故障，父类的修改影响子类几乎是必然的。

详见 [Calculator.java](CASE01/src/main/java/com/jahentao/IntegrationTest/case01/Calculator.java)
和 [RealTimeCalculator.java](CASE01/src/main/java/com/jahentao/IntegrationTest/case01/RealTimeCalculator.java)

可以根据提交版本记录，查看这个过程中的修改，体会修改父类的缘由，以及父类修改会影响子类。

- [修改前的场景](../../../commit/07d0e8d9c3f15033adc91aca91bd0b1e82df3a1e)
- [编写子类的开发者，修改子类，来解决发现的问题](../../../commit/ea6138f64953c21652d751cd319403b6b61fb559)
- [基类开发者修改基类，清除上次计算的影响](../../../commit/aa3b5131074bdb911bad4efe65b15f4f5b031b73)
- [基类开发者修改基类，将clear方法交给用户来使用，避免代码过多的侵入性](../../../commit/57b217da8a712c68115cbd6b1bce212a9a5d53a8)
- [编写子类的开发者，修改子类，不将clear的职责交给用户，替用户处理这个问题](../../../commit/0b3ccfd52df1d464c686f7613436ff5a9779bab4)


### 2. 多个模块共用公共数据

有的模块相依性是外在的，这种相依性与模块内部的实现机制无关，只是通过外部发生关联，典型的例子是共用公共数据。例如，模块A、B、C 共用公共数据 M，当模块 A 修改公共数据 M 时，会影响到使用这一公共数据的其他两个模块 B 和 C。如图所示。

![case02-模块间耦合](pic/case02-模块间耦合.png "case02-模块间耦合")

多个模块间有影响是正常的，不同模块之间的关系就是耦合，模块间追求低耦合。根据耦合程度可以分为7种，耦合度依次变低：

- 	内容耦合
- 	公共耦合
- 	外部耦合
- 	控制耦合
- 	标记耦合
- 	数据耦合
- 	非直接耦合


图示的情况是**公共耦合**（M为全局复杂数据结构）或**外部耦合**（M为全局简单变量），属于耦合度较高的，在设计上需要改进。

这里我想通过一个相关的例子，这是个典型的例子——**双重检查锁**，演示对内存中“**共享**”的“**公共数据**”修改，并没有在其他线程中及时反映的问题，也算是一种**修改公共数据的影响**。

这个问题基于JSR 133规范的 Java 内存模型，JDK版本必须是1.5及之后版本。涉及的全局公共数据在问题中，具体是一个**单例**，懒加载的单例模式，在单机的多线程并发（多模块间并发访问修改，外部耦合）的情景下，双重检查锁，确保数据的**可见性**和**一致性**。

在不考虑并发的情形下，懒加载单例模式是这样的：

详见 [Singleton.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/Singleton.java)

```
public class Singleton {

	private Singleton instance = null;

	public Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
}
```

而在并发情形下，你可以写成这样

详见 [Singleton2.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/concurrent/Singleton2.java)

```
public class Singleton2 {

	private volatile Singleton2 instance = null;

	public synchronized Singleton2 getInstance() {
		if (instance == null) {
			instance = new Singleton2();
		}
		return instance;
	}
}
```

把整个getInstance()方法设为同步，尽管这样做到了线程安全，并且解决了多实例问题，但并不**高效**。需要承受同步带来的性能开销，然而同步只在第一次调用的时候才被需要，也就是单例类实例创建的时候。
这促使使用双重检查锁模式，一种只在临界区代码加锁的方法。并且声明instance变量时使用volatile关键字。

详见 [Singleton.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/concurrent/Singleton.java)

```
public class Singleton {

	private volatile Singleton instance = null;

	public Singleton getInstance() {
		if (instance == null) {
			synchronized (this) {
				if (instance == null) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
}
```

**解释为什么要这样写，这个例子怎样体现对公共数据修改的影响**。

在双重检查锁中，代码会检查两次单例类是否有已存在的实例，一次加锁一次不加锁，一次确保不会有多个实例被创建。

第一个线程（可以理解为一个模块中）要获取该单例，进入同步锁代码区类，并创建了该单例。其他线程（可以理解为其他模块中）并发地也要获取该单例，没有volatile修饰符，可能出现Java中的另一个线程看到个**初始化**了一半的instance的情况。其他线程中保存的是一个**副本**，很可能会将单例又初始化了一遍。

这就是**可见性**，Java内存模型规定了所有的变量都存储在**主内存**中。每条线程中还有自己的**工作内存**，线程的工作内存中保存了被该线程所使用到的变量（这些变量是从主内存中**拷贝**而来）。线程对变量的所有操作（读取，赋值）都必须在工作内存中进行。不同线程之间也无法直接访问对方工作内存中的变量，线程间变量值的传递均需要通过主内存来完成。

![case02-主内存与工作内存](pic/case02-主内存与工作内存.png "case02-主内存与工作内存")

线程工作内存中的操作有：

- read and load 从主存复制变量到当前工作内存
- use and assign  执行代码，改变共享变量值
- store and write 用工作内存数据刷新主存相关内容

但是这一些操作并不是原子性，也就是 在read load之后，如果主内存count变量发生修改之后，线程工作内存中的值由于已经加载，不会产生对应的变化，所以计算出来的结果会和预期不一样。

![case02-变量在主内存和工作内存同步](pic/case02-变量在主内存和工作内存同步.png "case02-变量在主内存和工作内存同步")

回到例子，使用了volatile变量，保证了可见性，单例只会被初始化一次。volatile关键字保证**先行发生关系**，被volatile修饰的变量instance，所有的写都将先行发生于读，在Java 5之前不是这样，所以在这之前使用双重检查锁有问题。

我想这个例子来体现**修改公共数据的影响是比较深刻**的，平常的公共数据一般放在数据库中，由数据库来维护ACID特性。

并发总是会引入一些微妙错误，双重检查锁这个例子是有历史意义的。

编写一个多模块测试，验证对于公共数据，它们应该获得到的是相同的单例。

模块A 详见 [ModuleA.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/module/ModuleA.java)
模块B 详见 [ModuleB.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/module/ModuleB.java)
模块C 详见 [ModuleC.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/module/ModuleC.java)

测试详见 [App.java](CASE02/src/main/java/com/jahentao/integrationTest/case02/App.java)

```
public class App {

	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(3);
		// 这三个模块中获取的单例，内存地址应该是相同的
		pool.execute(new ModuleA());
		pool.execute(new ModuleB());
		pool.execute(new ModuleC());
		pool.shutdown();
	}

}
```
事实上，它们获取的单例，确实是相同的，都是在0x2e4d06a2。

![case02-多模块测试获取的是相同的单例](pic/case02-多模块测试获取的是相同的单例.png "case02-多模块测试获取的是相同的单例")

### 3. 一个模块可能对另一个模块产生不利的影响

例如，A 模块发送数据，B 模块接收数据并进行加工，B 模块缓冲区有限，如果 A 模块发送数据的速度快于 B 模块加工数据的速度，则两个模块集成起来后连续工作的时间长了，就会出现阻塞或者是数据丢失。

选的例子是[屏幕广播工具](https://gitee.com/jahentao/ScreenSAP)，以前做的一个小工具。服务器端录屏，服务器端**抓屏频率**会影响在客户端显示的问题，同时一个屏幕画面是拆成装进多个数据报中传输的，在客户端组合后，再显示出来。发送的数据包采用UDP协议，允许数据包有**丢失**，没有完整组合成一个画面帧的，与之相关的数据报被丢弃，和题目很契合。至于UDP的数据**缓冲**，那就是传输层、网络层的事了，应用层开发不关心。

所以一个模块的**参数选取**不当会对另一个模块产生不利的**影响**，以这个例子为例，就是**抓屏频率**，需要在实际环境下调整，实验结果100ms 抓一次屏幕比较合适，相应的客户端每100ms间隔刷新画面。

服务器端代码详见

[server包](CASE03/src/main/java/com/jahentao/integrationTest/case03/server)

[common包](CASE03/src/main/java/com/jahentao/integrationTest/case03/common)

客户端代码详见

[client包](CASE03/src/main/java/com/jahentao/integrationTest/case03/client)

运行

1) 打包出jar包文件`screenCAP-0.0.1-SNAPSHOT.jar`到当前路径下

2) 在客户端（如IP地址：192.168.237.1）运行，默认接收端口8888

```
java -cp screenCAP-0.0.1-SNAPSHOT.jar com.tjh.screenCAP.client.AppClient
```

3) 在服务器端（如IP地址：192.168.237.100的虚拟机中）运行，默认数据发送端口9999

```
java -cp screenCAP-0.0.1-SNAPSHOT.jar com.tjh.screenCAP.server.AppServer 192.168.237.1
```

案例效果如图。

![case03-屏幕广播工具效果](pic/case03-屏幕广播工具效果.png "case03-屏幕广播工具效果")

### 4. 将子功能组装时不一定产生所期望的主功能

有时因设计或者实现等原因，各子功能组装时并不能得到完整的主功能，而是可能会出现功能缺失，如下图所示。

![IntegrationTest-case04 功能缺失](pic/case04-功能缺失.png)

例如，某成绩管理软件，把成绩输入功能设计成两个子功能，输入百分制成绩和输入五级记分制成绩，
但集成测试时发现，这两项子功能合起来并不能覆盖所有可能的成绩输入，
因为成绩输入时还可能出现“缺考”、“作弊”等特殊情况。

这个例子，用JavaFX，在IDEA中用GUI Designer设计的页面，然后生成导出。当在eclipse中使用，项目需要依赖`IDEA_HOME\redist\forms_rt.jar`，已经放到`CASE04/libs/forms_rt.jar`。

```
<!-- 添加本地jar包 -->
<dependency>
    <groupId>com.intellij</groupId>
    <artifactId>local</artifactId>
    <version>1.0</version>
    <scope>system</scope>
    <systemPath>${basedir}/libs/forms_rt.jar</systemPath>
</dependency>
```

原项目来自[StudentGradesManageSystem](https://github.com/leftjs/StudentGradesManageSystem)，原项目使用的MySQL数据库，

为方便案例演示，改造成使用H2嵌入式数据库。数据库文件在`CASE04/db`下。同样需要说明的是，这个案例仅供演示，数据模型设计上从简，不适用于生产环境。

运行`Main.main`，效果如下。

系统用户有：管理员、教师、学生。


管理员登录，输入用户名`admin`，密码`123`

![case04-管理员登录](pic/case04-管理员登录.png "管理员登录")

管理员功能有：

课程及教师管理，对课程及教师信息的增删改

![case04-课程及教师管理](pic/case04-课程及教师管理.png "课程及教师管理")

学生信息管理，对学生信息的增删改

![case04-学生信息管理](pic/case04-学生信息管理.png "学生信息管理")

成绩录入维护，管理员查看老师录入成绩的状态（“已提交”，“已暂存”，默认未录入），可清除录入或者暂存状态

![case04-成绩录入维护](pic/case04-成绩录入维护.png "成绩录入维护")

管理员成绩查询，可查看到对应课程下已提交的学生成绩

![case04-管理员成绩查询](pic/case04-管理员成绩查询（有分数）.png "case04-管理员成绩查询（有分数）")

像“社会实践”及其他课程，教师成绩是**录入等级**，而非具体数值。所以，这就是**系统需求分析时**的功能缺陷。

![case04-管理员成绩查询2](pic/case04-管理员成绩查询（无分数）.png "case04-管理员成绩查询（无分数）")

成绩统计，可“按课程”、“按班级”、“按学生”、“按时间”分别统计

![case04-成绩统计](pic/case04-成绩统计.png "成绩统计")

以时间为例，统计大二下学期

![case04-成绩统计-按学年学期（大二学年，下学期）](pic/case04-成绩统计-按学年学期（大二学年，下学期）.png "成绩统计-按学年学期（大二学年，下学期）")

教师登录，输入用户名`teacher`，密码`123`

![case04-教师登录](pic/case04-教师登录.png "教师登录")

教师有成绩录入和个人信息管理两个功能。

![case04-教师成绩录入及查询](pic/case04-教师成绩录入及查询.png "教师成绩录入及查询")

成绩录入中途，可暂存学生成绩，等成绩都录入后提交。

教师只能录入具体数值的成绩，而无法录入等级。

这是由数据模型限制的

```
/**
学生成绩表
 */
DROP TABLE if EXISTS grade;
create TABLE grade(
id INT (11) NOT NULL PRIMARY KEY auto_increment,
courseId INT (11) NOT NULL ,
studentId INT (11) NOT NULL ,
score INT (8) NOT NULL,
FOREIGN KEY (courseId) REFERENCES course(id) on DELETE CASCADE ON UPDATE CASCADE ,
FOREIGN KEY (studentId) REFERENCES student(id) ON DELETE CASCADE ON UPDATE CASCADE
);
```

```
public class Grade {

    private int id;
    private int courseId;
    private int studentId;
    private int score;

    // getters and setters
}
```

学生登录，输入用户名`student`，密码`123`

![case04-学生登录](pic/case04-学生登录.png "学生登录")

学生有成绩查询、学生选课、个人资料管理三个功能。

![case04-学生成绩查询](pic/case04-学生成绩.png "成绩查询")

学生选中课程，确定选课，即可完成选课。

![case04-学生选课](pic/case04-学生选课.png "学生选课")

### 5. 独立可接受的误差，在多个模块组装后可能会被放大，超过可以接受的误差限度

#### 案例

设某模块 A 中变量 X 有误差 X，在后续的模块 B 中 对 X 进行了求立方运算，那么运算后的相对误差就是 3 倍的 X。因为 `Y = X^3`，两边微分得 `dY=3 X^2 dX`, 两边再同除以 Y 和 `X^3` 得： `dY/Y = 3 dX/X`。

App是一个应用，该应用处理过程中，会经过多个模块处理，得到最终结果

详见[App.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/App.java)

ModuleA 模块A 线性的误差

详见[ModuleA.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/ModuleA.java)

ModuleB 模块B 立方运算

详见[ModuleB.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/ModuleB.java)

用户使用该应用，详见单元测试[AppTest.java](CASE05/src/test/java/com/jahentao/integrationTest/test/case05/AppTest.java)

相对误差被放大

![IntegrationTest-case05](pic/case05-相对误差放大.png)

#### 另一个案例

某计算利息的程序，计算过程是模块 A 由年利率计算得出单日的利率 I_day，计算结果提供给模块 B 乘以金额和天数，得出总的利息 I_all。设年利率为 3%。如果 I_day 保留 5 位小数，则 I_day ≈ 0.00008，设某客户存款 1 亿元，存期 100 天，算出的利息为 80万元。如果 I_day 保留 7 位小数，则 I_day ≈ 0.0000822，算出的利息应为 82.2万元。这两者之间的误差达到了 2.2 万元。

利息计算程序 详见[InterestCalculationApp.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/other/InterestCalculationApp.java)

模块A 详见[ModuleA.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/other/ModuleA.java)

模块B 详见[ModuleB.java](CASE05/src/main/java/com/jahentao/integrationTest/case05/other/ModuleB.java)

单元测试 详见[InterestCalculationAppTest.java](CASE05/src/test/java/com/jahentao/integrationTest/test/case05/other/InterestCalculationAppTest.java)

```
  @Test
	public void testCalculate() {
		// 设年利率为 3%
		// 某客户存款 1 亿元，存期 100 天

		// 以保留5位小数计算
		double interest1 = app.calculate(0.03, 5, 100, 100000000);
		BigDecimal b1 = new BigDecimal(interest1);
		double result1 = b1.setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue(); // 计算结果保留5位小数，
																				// 保持前后精度相同
		System.out.println(result1); // 800000.0  80万

		// 以保留7位小数计算
		double interest2 = app.calculate(0.03, 7, 100, 100000000);
		BigDecimal b2 = new BigDecimal(interest2);
		double result2 = b2.setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue(); // 计算结果保留7位小数，
																				// 保持前后精度相同
		System.out.println(result2); // 822000.0  82.2万

		System.out.println(result2 - result1); // 相差 2.2 万
	}
```

### 6. 可能会发现单元测试中未发现的接口方面的错误

例如模块 A 有两个形式参数 str1，str2，功能是实现把 str1 中包含的 str2 去掉后保存。
模块 B 调用模块 A 时参数位置写反了，把原始字符串作为第二个参数，而把要删除的字符串作为了第一个参数。
这种情况单元测试时有可能没有发现，因为单元测试主要关注模块内部的具体实现，而通过集成测试是可以发现的。

模块A 详见[ModuleA.java](CASE06/src/main/java/com/jahentao/integrationTest/case06/ModuleA.java)

模块A的单元测试，无问题 详见[ModuleATest.java](CASE06/src/test/java/com/jahentao/integrationTest/case06/ModuleATest.java)

模块B 详见[ModuleB.java](CASE06/src/main/java/com/jahentao/integrationTest/case06/ModuleB.java)

模块B调用模块A，模块B集成模块A，对模块B功能进行测试 详见[ModuleBTest.java](CASE06/src/test/java/com/jahentao/integrationTest/case06/ModuleBTest.java)

### 7. 在单元测试中无法发现时序问题

在程序并发中，很容易出现时序问题。单元测试时一个一个模块单独执行，相互之间没有影响，测试运行可能都没有发现问题，但集成测试时，多个模块并发执行，操作的次序存在不确定性，如果对时序问题考虑不周，就会出现错误。
以购票软件为例，如果没有做好并发控制，当购票和退票模块并发执行时，如果出现如图所示情形，软件就会错。

![case07-并发时序问题](pic/case07-并发时序问题.png "并发时序问题")

购票模块执行过程中，并发执行了退票模块，两个模块都读取到了相同的初始“票数 X”，退票模块执行后，“票数 X”加了 1，但由于购票模块已经读取过了“票数”，所以这一修改并没有更新到购票模块。购票操作执行后, 票数 X=X-1 被写回到了数据池，这样就出错了。

正常结果应当是：余票  X = X+1-1 退回一张票卖出一张票，余票仍然 = X；而这种情况下实际结果是： 余票  X = X-1 少了一张票，退回的票“丢失了”！

详见代码

模拟应用 [App.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/error/App.java)

票 [Ticket.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/error/Ticket.java)

购票线程 [BookTicketThread.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/error/BookTicketThread.java)

退票线程 [RefundTicketThread.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/error/RefundTicketThread.java)

运行App.main
初始的时候余票数为100

![case07-时序问题](pic/case07-时序问题.png "运行时序问题")

以上代码没进行并发处理，在并发情境下会发生多种错误。比如购多张票，多个线程购票退票，以上程序都会出现各种问题。

正确的并发处理，方法很多，简单地，添加一个同步锁即可。详见代码
[BookTicketThread.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/correct/BookTicketThread.java)。
[RefundTicketThread.java](CASE07/src/main/java/com/jahentao/integrationTest/case07/correct/RefundTicketThread.java)

### 8. 在单元测试中无法发现资源竞争问题

例如，A 模块和 B 模块运行时都同时需要资源 X 和 Y，当前运行环境下资源 X 和 Y 各有 1 个，A 模块先申请到了资源 X ，B 模块先申请到了资源 Y，然后 A 模块等待资源 Y，B 模块等待资源 X， A、B 陷入死锁，如图所示 。

![case08-因资源竞争而产生死锁](pic/case08-因资源竞争而产生死锁.png "因资源竞争而产生死锁")

资源竞争是操作系统层面，为提高资源利用率，而进行并发控制中，不可避免的有资源竞争，关键看资源竞争中进程调度是否产生了死锁。

破坏资源竞争，就是破坏造成死锁的四个条件中的，（资源）不可剥夺条件。

先写了一个案例，代码中，以享元模式设计资源池。

详见 [Pool.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/Pool.java)

```
public class Pool<T extends Resource> {

	private int maxAvailable;
	private Semaphore available;

	public Pool(int maxAvailable) {
		this.maxAvailable = maxAvailable;
		available = new Semaphore(maxAvailable, true); // 信号量
		used = new boolean[maxAvailable];
	}

	public T getItem() throws InterruptedException {
		available.acquire();
		return getNextAvailableItem();
	}

	public void putItem(T x) {
		if (markAsUnused(x))
			available.release();
	}

	protected T[] items;

	public T[] getItems() {
		return items;
	}

	public void setItems(T[] items) {
		this.items = items;
	}

	protected boolean[] used;

	protected synchronized T getNextAvailableItem() {
		for (int i = 0; i < maxAvailable; ++i) {
			if (!used[i]) {
				used[i] = true;
				return items[i];
			}
		}
		return null;
	}

	protected synchronized boolean markAsUnused(T item) {
		for (int i = 0; i < maxAvailable; ++i) {
			if (item == items[i]) {
				if (used[i]) {
					used[i] = false;
					return true;
				} else
					return false;
			}
		}
		return false;
	}
}

```

资源 详见 [Resource.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/Resource.java)、
[ResourceX.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/ResourceX.java)
和[ResourceY.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/ResourceY.java)

模块A 详见 [ModuleA.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/ModuleA.java)
和 模块B [ModuleB.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/ModuleB.java)

应用模拟 [App.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/App.java)

eclipse运行结果，死锁（程序一直不终止），运行效果

![case08-死锁](pic/case08-死锁.png)

以上的例子，体现了死锁的概念。但是由于通过使用Semaphore信号量来实现，acquire方法以阻塞式的方式获取许可。实际上，线程在没有申请到资源时，是
阻塞，等待状态（Waiting），等待其他资源release释放许可，而非阻塞（Blocked）状态。

操作系统线程是三种基本状态，Java中对线程状态的建模是五种状态，如下：

![case08-Java中线程状态](pic/case08-Java中线程状态.png "Java中线程状态")

简而言之，上述代码的并发协同，还不是真正意义上的死锁。
使用JDK的工具JConsole，可以看出Waiting状态。

![case08-JConsole-1-Thread-0-Waitring](pic/case08-JConsole-1-Thread-0-Waitring.png "case08-JConsole-1-Thread-0-Waitring")

![case08-JConsole-1-Thread-1-Waitring](pic/case08-JConsole-1-Thread-1-Waitring.png "case08-JConsole-1-Thread-1-Waitring")

一次性竟没写出真正的死锁，不过，不完整正确的例子也是有意义的。要说明的是，上面的例子是死锁，但没精确的达到演示效果。

再写了个例子，纯粹使用synchronized来加同步锁，锁对象。

```
public class SyncDeadLock{
    private static Object locka = new Object();
    private static Object lockb = new Object();

    public static void main(String[] args){
        new SyncDeadLock().deadLock();
    }

    private void deadLock(){
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (locka){
                    try{
                        System.out.println(Thread.currentThread().getName()+" get locka ing!");
                        Thread.sleep(500);
                        System.out.println(Thread.currentThread().getName()+" after sleep 500ms!");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName()+" need lockb!Just waiting!");
                    synchronized (lockb){
                        System.out.println(Thread.currentThread().getName()+" get lockb ing!");
                    }
                }
            }
        },"thread1");

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lockb){
                    try{
                        System.out.println(Thread.currentThread().getName()+" get lockb ing!");
                        Thread.sleep(500);
                        System.out.println(Thread.currentThread().getName()+" after sleep 500ms!");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName()+" need locka! Just waiting!");
                    synchronized (locka){
                        System.out.println(Thread.currentThread().getName()+" get locka ing!");
                    }
                }
            }
        },"thread2");

        thread1.start();
        thread2.start();
    }
}
```

详见 [SyncDeadLock.java](CASE08/src/main/java/com/jahentao/integrationTest/case08/demo/SyncDeadLock.java)

使用JConsole,检测死锁，可见死锁Blocked状态，达到案例预期的效果。

![case08-JConsole-2-thread1-Blocked](pic/case08-JConsole-2-thread1-Blocked.png "case08-JConsole-2-thread1-Blocked")

![case08-JConsole-2-thread2-Blocked](pic/case08-JConsole-2-thread2-Blocked.png "case08-JConsole-2-thread2-Blocked")


### 9. 共享数据或全局数据的问题

例如，成绩管理软件中，成绩是全局数据，在输入模块获得初值后传入统计模块进行统计。
但集成测试时发现，输入模块为方便接收各种不同类型的成绩，把成绩数据默认为字符类型，
而在统计模块，为便于计算，把成绩数据默认为数值类型，这样两个模块一对接，就出现了数据类型不一致的问题。

案例修改自4、成绩管理系统的案例。把成绩数据改为字符类型，可录入成绩登记信息，如“优秀”、“良好”等。

数据模型修改

```
/**
学生成绩表
 */
DROP TABLE if EXISTS grade;
create TABLE grade(
id INT (11) NOT NULL PRIMARY KEY auto_increment,
courseId INT (11) NOT NULL ,
studentId INT (11) NOT NULL ,
score CHAR (4) NOT NULL,
FOREIGN KEY (courseId) REFERENCES course(id) on DELETE CASCADE ON UPDATE CASCADE ,
FOREIGN KEY (studentId) REFERENCES student(id) ON DELETE CASCADE ON UPDATE CASCADE
);
```

```
public class Grade {

    private int id;
    private int courseId;
    private int studentId;
    private String score;

    // getters and setters
}
```

![case09-管理员成绩查询（可填入等级）](pic/case09-管理员成绩查询（可填入等级）.png "管理员成绩查询（可填入等级）")

现在统计模块，绘制饼状图，使用的jfreechart的jar包。这是个第三方的类库，所以出现数据类型不一致的问题也正常。

饼状图DefaultPieDataset的API不接收字符类型数据，如下表（只接受double类型和Number类型），且“优秀”、“良好”等字符串不能转数字，这样两个模块一对接，就出现了数据类型不一致的问题。

返回值 | 方法 | 描述
--|--|--
void | setValue(Comparable key, double value) | Sets the data value for a key and sends a DatasetChangeEvent to all registered listeners.
void | setValue(Comparable key, Number value) | Sets the data value for a key and sends a DatasetChangeEvent to all registered listeners.

在统计模块，要绘制饼状图，遇到这种**强**数据类型不一致，只能在逻辑上变通去处理。

比如在数据库服务器端定义一个函数，GET_SCORE将score转换为数值。

首先规定

不及格 | 及格 | 中等 | 良好 | 优秀
--|--|--|--|--
[0,60) | [60,70) | [70,80) | [80,90) | [90,100]

H2数据库支持从Java源码创建函数

```
CREATE ALIAS GET_SCORE AS $$
int getScore(String score) {
    switch (score) {
        case "不及格": return 50;
        case "及格": return 65;
        case "中等": return 75;
        case "良好": return 85;
        case "优秀": return 95;
        default : return Integer.parseInt(score);
    }
}
$$;
```

![case09-get_score函数](pic/case09-定义get_score函数.png "get_score函数")

详见[StatisticUtils.java](CASE09/src/main/java/com/jahentao/integrationTest/case09/utils/StatisticUtils.java)


### 10. 数据单位、环境参数统一的问题

例如某收费软件系统中有两个模块单元，一个称重，一个计费，但称重模块中重量单位为克，而计费模块中把重量的数据单位默认为公斤，这样当称重结果数据传到计费模块后，费用的计算结果肯定是错误的。

![因默认数据单位不一致而导致错误](pic/case10-因默认数据单位不一致而导致错误.png "因默认数据单位不一致而导致错误")

称重模块 详见[WeightingModule.java](CASE10/src/main/java/com/jahentao/integrationTest/case10/WeightingModule.java)

计费模块 详见[ChargingModule.java](CASE10/src/main/java/com/jahentao/integrationTest/case10/ChargingModule.java)

该收费软件 详见[App.java](CASE10/src/main/java/com/jahentao/integrationTest/case10/App.java)

单元测试 详见[AppTest.java](CASE10/src/test/java/com/jahentao/integrationTest/test/case10/AppTest.java)

```
  @Test
	public void testRun() {
		// 原本以为12 元，然而计算得出的结果为需要缴费12000元
		assertEquals(Double.doubleToLongBits(12),  // 转换为长整型，比较两个双精度浮点数
				Double.doubleToLongBits(app.run()));
	}
```

### 11. 多个模块因调用 API 而产生影响

调用API的**影响**这东西，可有可无，有的没的。不知道问题情境是想表达什么。

该案例参见2、多个模块共用公共数据。

用数据，就是调用数据有关的API。

### 12. 多个客户端共用服务器对象

这里以一个聊天室程序为例，多个客户端参与聊天，聊天消息由服务器广播给各参与客户端。聊天室采用的通信协议为WebSocket协议，基于RFC 6455规范。

服务器详见 [ChatServer.java](CASE12/src/main/java/com/jahentao/integrationTest/case12/ChatServer.java)

客户端详见 [ChatClient.java](CASE12/src/main/java/com/jahentao/integrationTest/case12/ChatClient.java)

首先要启动聊天服务器ChatServer，默认端口8887。

![case12-启动服务器](pic/case12-启动服务器.png "case12-启动服务器")

然后才能启动客户端，使用设置的默认服务器URI地址：`ws://localhost:8887`。

![case12-启动客户端](pic/case12-启动客户端.png "case12-启动客户端")

可启动多个客户端，连接服务器后，在聊天室中聊天。聊天服务器将接收到的消息广播给连接到聊天室中的客户端。

![case12-聊天室聊天](pic/case12-聊天室聊天.png "case12-聊天室聊天")

### 13. 两个模块之间因消息传递而产生影响

多模块（或多线程）间消息传递一开始就应该约定好格式，比如进程间通信，数据包头部定义。

选的例子是多线程下载工具，以前做的一个小工具。该下载工具实现了多线程并发下载、断点续传、保存未完成的下载任务等功能，目前只支持HTTP协议，暂不支持配置cookie等信息，重定向，在连接被断开时主动重连等，这些都是应用场景下还要逐步完善的。

案例演示**消息传递**中的请求数据头部的相关属性。相应的，如果不在多模块间规定**消息传递**，就会产生所写数据的覆盖、错乱等**错误影响**。

原理是这样的，根据URL向服务器请求文件，**请求头指定范围range**，`range`头域可以请求实体的一个或者多个子范围。服务器接收到线程的请求报文，发现这是一个带有`range`头的GET请求，如果一切正常，服务器的响应报文会有`HTTP/1.1 206 OK`表示处理请求成功。

客户端根据线程数将文件进行切分，给每个下载线程分配一个范围`[start,end)`。如下：

![多线程下载范围划分例子](pic/case13-多线程下载范围划分例子.png "多线程下载范围划分例子")

以线程0为例，它的请求头就是`Range: bytes=0-62059459`

每个线程在下载过程中，记录下载的字节数`amount`，并且在文件下载过程中更新记录文件`*.download`，记录每个线程的下载情况。记录文件，如下所示：

![case13-下载信息记录文件](pic/case13-下载信息记录文件.png "case13-下载信息记录文件")

保存记录文件，可供停止任务后，下次断点续传，继续中断的下载任务。
具体详见代码。

[App.java](CASE13/src/main/java/com/jahentao/integrationTest/case13/App.java)

[DownloadInfo.java](CASE13/src/main/java/com/jahentao/integrationTest/case13/DownloadInfo.java)

[DownloadService.java](CASE13/src/main/java/com/jahentao/integrationTest/case13/DownloadService.java)

[DownloadThread.java](CASE13/src/main/java/com/jahentao/integrationTest/case13/DownloadThread.java)

[MultiThreadDownloadJFrame.java](CASE13/src/main/java/com/jahentao/integrationTest/case13/MultiThreadDownloadJFrame.java)

使用例子，比如我要下载Nvidia最近推出GeForce 397.64显卡驱动，对应的Win10移动显卡下载地址是：
```
http://cn.download.nvidia.com/Windows/397.64/397.64-notebook-win10-64bit-international-whql.exe
```
这个文件下载基于HTTP协议，且不需要cookie等认证，可以直接使用自己做的下载工具。

![case13-下载界面1](pic/case13-下载界面1.png "case13-下载界面1")

填个8线程，主要是因为我的笔记本CPU i76700HQ就是四核八线程啦。

如果填的线程数过多，比如32线程，有些服务器对单一客户端所能使用的最大连接数做了限制，当连接数过多，服务器会将电脑列入黑名单；此外，对于某些类型的连接或性能较低的电脑、路由器，当增加连接数时，下载速度反而会下降。

![case13-下载界2](pic/case13-下载界面2.png "case13-下载界面2")

![case13-下载界面3](pic/case13-下载界面3.png "case13-下载界面3")
