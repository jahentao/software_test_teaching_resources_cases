/**
 * 下载信息的实体类
 */
package com.jahentao.integrationTest.case13;

import java.io.File;
import java.net.URL;

public class DownloadInfo {
	private URL url; //URL资源文件
	private String outPath; //输出目录
	private File metadataFile; // 元数据文件
	private int threadNum; //线程数
	private long start;	//开始位置
	private long end;	//结束位置
	private long amount; //已经复制的数量
	private int index; // 线程标号
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public String getOutPath() {
		return outPath;
	}
	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}
	public int getThreadNum() {
		return threadNum;
	}
	public void setThreadNum(int threadNum) {
		this.threadNum = threadNum;
	}
	public long getStart() {
		return start;
	}
	public void setStart(long start) {
		this.start = start;
	}
	public long getEnd() {
		return end;
	}
	public void setEnd(long end) {
		this.end = end;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public File getMetadataFile() {
		return metadataFile;
	}
	public void setMetadataFile(File metadataFile) {
		this.metadataFile = metadataFile;
	}
}
