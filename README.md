# 软件测试教学资源案例

#### 项目介绍
软件测试教学资源案例

优课在线 [《软件质量保证与测试》](http://www.uooconline.com/course/1420294878)

准备的案例分为[单元测试](UnitTest/README.md)和[集成测试](IntegrationTest/README.md)两部分

#### 说明

##### 软件管理说明

- 使用Maven多模块管理项目
- 使用Git进行版本控制，托管在[码云](https://gitee.com/jahentao/software_test_teaching_resources_cases)上

##### 项目环境说明

- JDK 8
- Maven 3.3.9
- Eclipse Oxygen.2
- JUnit 4.12

#### 安装教程

1. 确保安装好了Java，并配置好环境变量，安装好Eclipse。

2. 安装Maven。

[下载页面](https://maven.apache.org/download.cgi)，版本和[Java依赖](https://maven.apache.org/docs/history.html)，确保Java 7及以上环境已配置。

以v3.5.3为例，[下载链接](http://mirror.cc.columbia.edu/pub/software/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz)

下载二进制发布版，解压到指定文件夹，如`C:\Program Files\MyProgram\apache-maven-3.5.3`。

配置环境变量，如图

![maven安装-环境变量配置](UnitTest/pic/maven安装-环境变量配置.png "maven安装-环境变量配置")

然后编辑Path环境变量，新增

![maven安装-Path新增路径](UnitTest/pic/maven安装-Path新增路径.png "maven安装-Path新增路径")

打开cmd，验证

![maven安装-验证](UnitTest/pic/maven安装-验证.png "maven安装-验证")

有关在Eclipse中使用Maven构建项目，可参考[博客](https://my.oschina.net/u/2605948/blog/698725)

3. 导入eclipse

导入方式很多，以下详述一种方式：

![导入eclipse-Step1](UnitTest/pic/导入eclipse-Step1.png "导入eclipse-Step1")

![导入eclipse-Step2](UnitTest/pic/导入eclipse-Step2.png "导入eclipse-Step2")

![导入eclipse-Step3](UnitTest/pic/导入eclipse-Step3.png "导入eclipse-Step3")

![导入eclipse-Step4](UnitTest/pic/导入eclipse-Step4.png "导入eclipse-Step4")

![导入eclipse-Step5](UnitTest/pic/导入eclipse-Step5.png "导入eclipse-Step5")

![导入eclipse-Step6](UnitTest/pic/导入eclipse-Step6.png "导入eclipse-Step6")

![导入eclipse-Step7](UnitTest/pic/导入eclipse-Step7.png "导入eclipse-Step7")

![导入eclipse-Step8](UnitTest/pic/导入eclipse-Step8.png "导入eclipse-Step8")

![导入eclipse-Step9](UnitTest/pic/导入eclipse-Step9.png "导入eclipse-Step9")

![导入eclipse-Step10](UnitTest/pic/导入eclipse-Step10.png "导入eclipse-Step10")

#### 使用说明

1. 参考README说明和教师讲解
2. 运行程序、调试修改
3. 思考体会

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
